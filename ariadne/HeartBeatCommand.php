<?php

namespace Ariadne;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Bluerhinos\phpMQTT;

class HeartBeatCommand extends Command {

	protected function configure(){
		$this->setName("Ariadne:HeartBeat")->setDescription("HeartBeat no MQTT"); 
	}

	protected function execute(InputInterface $input, OutputInterface $output){

		$this->publish([
			['topic' => 'supervisor-heartbeat', 'message' => @date()]
		]);

	}

	private function publish(array $messages = []) {
		$mqttClient = new phpMQTT("35.181.11.230", 1883, "Slim");
		// $_publish[] = ['messages' => $messages, 'phpMQTT' => $mqttClient];
		if ($mqttClient->connect(false)) {
			foreach($messages as $message) {
				$_publish[] = ['message' => $message];
				$mqttClient->publish($message['topic'], $message['message'], 1);
			}
			$mqttClient->close();
			$mqttClient->disconnect();
		}
		sleep(60);
	}

}
