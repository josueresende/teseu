-- 2019-06-01
CREATE TABLE `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `data_criacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_delecao` datetime DEFAULT NULL,
  `acao` longtext,
  `hash` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- 2019-06-02
CREATE TABLE `auditoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `acao` longtext,
  `log` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- 2019-06-05
ALTER TABLE `localizacao`
  ADD COLUMN `tipo` VARCHAR(128) NOT NULL DEFAULT 'INDEFINIDO';
-- 2019-06-10
ALTER TABLE `usuario`
  ADD COLUMN `login` VARCHAR(128) NOT NULL DEFAULT '';
