openssl req -x509 -nodes -newkey rsa:1024 -days 1 -keyout '/etc/letsencrypt/live/teseu.pasifae.name/privkey.pem' -out '/etc/letsencrypt/live/teseu.pasifae.name/fullchain.pem' -subj '/CN=teseu.pasifae.name'
certbot certonly --webroot -w /var/www/certbot --register-unsafely-without-email -d teseu.pasifae.name --rsa-key-size 4096 --agree-tos --force-renewal

openssl req -x509 -nodes -newkey rsa:4096 -days 30 -keyout "/etc/letsencrypt/live/teseu.pasifae.name/privkey.pem" -out "/etc/letsencrypt/live/teseu.pasifae.name/fullchain.pem" -subj "/C=US/O=Let's Encrypt/CN=Let's Encrypt Authority X3/CN=teseu.pasifae.name"
certbot certonly --agree-tos --force-renewal --rsa-key-size 4096 --webroot -w /var/www/certbot --email josue.resende@gmail.com --domains "teseu.pasifae.name" 

