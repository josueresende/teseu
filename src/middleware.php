<?php
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);
/**
 * Example middleware closure
 *
 * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
 * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
 * @param  callable                                 $next     Next middleware
 *
 * @return \Psr\Http\Message\ResponseInterface
 */
$app->add(function (ServerRequestInterface $request, ResponseInterface $response, $next) {

    $sessions = array();
    if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));

    $queryContents = $request->getUri()->getQuery();
    $bodyContents = $request->getBody()->getContents();

    $signH = $request->hasHeader('SIGN_H') ? strtr($request->getHeader('SIGN_H')[0], '-_', '+/') : NULL;
    $signQ = $request->hasHeader('SIGN_Q') ? strtr($request->getHeader('SIGN_Q')[0], '-_', '+/') : NULL;
    $signB = $request->hasHeader('SIGN_B') ? strtr($request->getHeader('SIGN_B')[0], '-_', '+/') : NULL;

    $uid = $request->hasHeader('UID') ? ($request->getHeader('UID')[0]) : NULL;
    $heartBeat = $request->hasHeader('HEARTBEAT') ? ($request->getHeader('HEARTBEAT')[0]) : NULL;

    $qtdQueryParams = count($request->getQueryParams());
    $qtdPostParams = count($request->getParsedBody());

    error_log(var_export([
        __FILE__ => __LINE__,
        // 'baseUrl' => $request->getUri()->getBaseUrl(),
        // 'getBasePath' => $request->getUri()->getBasePath(),
        'getPath' => $request->getUri()->getPath(),
        'getQuery' => $request->getUri()->getQuery(),
        '$qtdQueryParams' => $qtdQueryParams,
        '$request->getQueryParams()' => $request->getQueryParams(),
        '$qtdPostParams' => $qtdPostParams,
        '$request->getParsedBody()' => $request->getParsedBody(),
    ], true));

    $log = [];

    // error_log(var_export([
    //   // '$uri' => => $request->getUri(),
    //   // '$request->getHeaders()' => $request->getHeaders(),
    //   'getBody.getContents' => $bodyContents,
    //   'SIGN_Q' => $signQ,
    //   'SIGN_B' => $signB,
    //   'UID' => $uid,
    // ], true));

    if (empty($uid)) {
        $request = $request->withAttribute('VALIDATION', false);

        $middleware = $next($request, $response);

    } else if (@array_key_exists($uid, $sessions)) {

        $request = $request->withAttribute('VALIDATION', true);

        {

            if ($request->isGet() || $request->isDelete()) {
                if ($qtdQueryParams > 0 && is_null($signQ)) return $response->withStatus(405);
            } else if ($request->isPost()){
                if ($qtdPostParams > 0 && is_null($signB)) return $response->withStatus(406);
            }
        }

        $session = $sessions[$uid];

        $request = $request->withAttribute('ID_USUARIO', $session['id_usuario']);
        $request = $request->withAttribute('TP_USUARIO', $session['tp_usuario']);

        $verify = false;
        $signature = '';

        {
            $publicKey = strtr($session['remote.public'], '-_', '+/');
            $rsaPublicKey = new RSA();
            $rsaPublicKey->setPublicKeyFormat(RSA::PUBLIC_FORMAT_PKCS8);
            $rsaPublicKey->setHash('sha256');
            $rsaPublicKey->loadKey($publicKey);
            $rsaPublicKey->setSignatureMode(RSA::SIGNATURE_PKCS1);
            $rsaPublicKey->setEncryptionMode(RSA::ENCRYPTION_PKCS1);

            $verify = false;

            if (($qtdQueryParams > 0) && ($request->isGet() || $request->isDelete())) {
              $queryContents = urldecode($queryContents);
              $signature = base64_decode($signQ);
              $verify = $rsaPublicKey->verify($queryContents, $signature);
              $log = [
                'verify' => $verify,
                'query' => $queryContents,
                'UID' => $uid,
                'SIGN_H' => $signH,
                'SIGN_Q' => $signQ,
                'SIGN_B' => $signB,
              ];
            } else if (($qtdPostParams > 0) && $request->isPost()) {
              $signature = base64_decode($signB);
              $verify = $rsaPublicKey->verify($bodyContents, $signature);
              $log = [
                'verify' => $verify,
                'body' => $bodyContents,
                'UID' => $uid,
                'SIGN_H' => $signH,
                'SIGN_Q' => $signQ,
                'SIGN_B' => $signB,
              ];
            } else {
              $signature = base64_decode($signH);
              $verify = $rsaPublicKey->verify($heartBeat, $signature);
              $log = [
                'verify' => $verify,
                'heartBeat' => $heartBeat,
                'UID' => $uid,
                'SIGN_H' => $signH,
                'SIGN_Q' => $signQ,
                'SIGN_B' => $signB,
              ];
            }

            error_log(var_export([
                __FILE__ => __LINE__,
                'getPath' => $request->getUri()->getPath(),
                'getQuery' => $request->getUri()->getQuery(),
                '$log' => $log,
            ], true));

            if ($verify == false) {
              return $response->withStatus(404);
            }

            $request = $request->withAttribute('VERIFIED', true);
        }

        $middleware = $next($request, $response);

        $length = $middleware->getBody()->getSize();

        $middleware->getBody()->rewind();

        if ($length > 0) {
            $content = $middleware->getBody()->read($length);

            $rsaPrivateKey = new RSA();
            $rsaPrivateKey->setPublicKeyFormat(RSA::PUBLIC_FORMAT_PKCS8);
            $rsaPrivateKey->setHash('sha256');
            $rsaPrivateKey->loadKey($session['local.private']);
            $rsaPrivateKey->setSignatureMode(RSA::SIGNATURE_PKCS1);
            $rsaPrivateKey->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
            $signature = $rsaPrivateKey->sign($content);

            $signature = base64_encode($signature);

            $signature = strtr($signature, '+/', '-_');

            // error_log(var_export(['$log' => $log, '$signature' => $signature], true));

            $middleware = $middleware->withHeader('SIGN_B', $signature);
        }
    } else {

      $request = $request->withAttribute('VALIDATION', false);
      $middleware = $next($request, $response);

    }


    // error_log(var_export(['$log' => $log, '$middleware' => $middleware], true));
    // error_log(var_export(['$log' => $log, '$middleware->getBody()' => $middleware->getBody()], true));
    // error_log(var_export(['$log' => $log, '$content' => $content], true));
    // error_log(var_export(['$log' => $log, '$middleware->getBody()->getContents()' => $middleware->getBody()->getContents()], true));

    return $middleware;
});
