<?php
$sql_mensagens_pesquisador = "
SELECT
mensagem.id AS mensagem_id,
mensagem_direta.id AS mensagem_direta_id,
mensagem_direta.id_mensagem_referencia AS mensagem_id_referencia,
mensagem.conteudo AS mensagem_conteudo,
mensagem.extra AS mensagem_extra,
mensagem.data_criacao AS mensagem_data_criacao,
usuario.id AS autor_id,
usuario.nome AS autor_nome,
usuario.tipo AS autor_tipo,
usuario_direta.id AS leitor_id,
usuario_direta.nome AS leitor_nome,
usuario_direta.tipo AS leitor_tipo,
mensagem_anuncio.id AS anuncio_id,
mensagem_anuncio.data_expiracao AS anuncio_data_expiracao,
busca.id AS busca_id
FROM mensagem
LEFT JOIN usuario ON usuario.id = mensagem.id_usuario
LEFT JOIN mensagem_direta ON mensagem_direta.id_mensagem = mensagem.id
LEFT JOIN usuario usuario_direta ON usuario_direta.id = mensagem_direta.id_usuario
LEFT JOIN mensagem_anuncio ON mensagem_anuncio.id_mensagem = mensagem.id AND mensagem_anuncio._deleted = 'N'
LEFT JOIN busca ON busca.id = mensagem_anuncio.id_busca
WHERE mensagem.data_delecao IS NULL
AND (
mensagem.id_usuario = :id_usuario OR
mensagem.id IN (SELECT id_mensagem FROM mensagem_direta WHERE id_usuario = :id_usuario)
)
AND 1=1
ORDER BY mensagem.data_criacao DESC
";
// ----------------
$sql_mensagens_voluntario = "
SELECT
mensagem.id AS mensagem_id,
mensagem_direta.id AS mensagem_direta_id,
mensagem_direta.id_mensagem_referencia AS mensagem_id_referencia,
mensagem.conteudo AS mensagem_conteudo,
mensagem.extra AS mensagem_extra,
mensagem.data_criacao AS mensagem_data_criacao,
usuario.id AS autor_id,
usuario.nome AS autor_nome,
usuario.tipo AS autor_tipo,
usuario_direta.id AS leitor_id,
usuario_direta.nome AS leitor_nome,
usuario_direta.tipo AS leitor_tipo,
mensagem_anuncio.id AS anuncio_id,
mensagem_anuncio.data_expiracao AS anuncio_data_expiracao,
busca.id AS busca_id
FROM mensagem
LEFT JOIN usuario ON usuario.id = mensagem.id_usuario
LEFT JOIN mensagem_direta ON mensagem_direta.id_mensagem = mensagem.id
LEFT JOIN usuario usuario_direta ON usuario_direta.id = mensagem_direta.id_usuario
LEFT JOIN mensagem_anuncio ON mensagem_anuncio.id_mensagem = mensagem.id AND now() < mensagem_anuncio.data_expiracao AND mensagem_anuncio._deleted = 'N'
LEFT JOIN busca ON busca.id = mensagem_anuncio.id_busca
WHERE mensagem.data_delecao IS NULL
AND (
    mensagem.id_usuario = :id_usuario
    OR mensagem.id IN (SELECT id_mensagem FROM mensagem_direta WHERE id_usuario = :id_usuario)
    OR mensagem_anuncio.id IN (
        SELECT id_anuncio -- ,count(id_buscada),count(id_marcada)
        FROM (
            SELECT
                resposta.id AS id_resposta,
                resposta_buscada.id_mensagem_anuncio AS id_anuncio,
                resposta_buscada.id AS id_buscada,
                resposta_marcada.id AS id_marcada
            FROM resposta
            LEFT JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id AND resposta_marcada.id_usuario = :id_usuario
            INNER JOIN resposta_buscada ON resposta_buscada.id_resposta = resposta.id
            WHERE resposta_buscada._deleted = 'N'
                AND resposta.conteudo = 'MARCACAO'
                AND resposta_buscada.id_mensagem_anuncio IS NOT NULL
        UNION ALL
            SELECT
                resposta.id AS id_resposta,
                resposta_buscada.id_mensagem_anuncio AS id_anuncio,
                resposta_buscada.id AS id_buscada,
                resposta_marcada.id AS id_marcada
            FROM resposta
            LEFT JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id AND resposta_marcada.id_usuario = :id_usuario
            INNER JOIN resposta_buscada ON resposta_buscada.id_resposta = resposta.id
            WHERE resposta_buscada._deleted = 'N'
                AND resposta_buscada.id_mensagem_anuncio IS NOT NULL
                AND resposta.conteudo = 'DATA'
                AND TIMESTAMPDIFF(YEAR,STR_TO_DATE(resposta_marcada.texto,'%Y-%m-%d'),CURDATE())
                    BETWEEN JSON_EXTRACT(resposta_buscada.texto, '$.min') AND JSON_EXTRACT(resposta_buscada.texto, '$.max')
        UNION ALL
            SELECT
                resposta.id AS id_resposta,
                resposta_buscada.id_mensagem_anuncio AS id_anuncio,
                resposta_buscada.id AS id_buscada,
                resposta_marcada.id AS id_marcada
            FROM resposta
            LEFT JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id AND resposta_marcada.id_usuario = :id_usuario
            INNER JOIN resposta_buscada ON resposta_buscada.id_resposta = resposta.id
            WHERE resposta_buscada._deleted = 'N'
                AND resposta_buscada.id_mensagem_anuncio IS NOT NULL
                AND resposta.conteudo = 'DURACAO_MESES'
                AND TIMESTAMPDIFF(MONTH,STR_TO_DATE(resposta_marcada.texto,'%Y-%m-%d'),CURDATE())
                    BETWEEN JSON_EXTRACT(resposta_buscada.texto, '$.min') AND JSON_EXTRACT(resposta_buscada.texto, '$.max')
        ) XPTO_1
        GROUP BY id_anuncio
        HAVING count(id_buscada) = count(id_marcada)
    )
)

ORDER BY mensagem.data_criacao DESC
";
