<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$_test_mqtt_publish = [];
$app->get('/test', function (Request $request, Response $response, array $args) use ($app) {
  $data = [];
  $pid = pcntl_fork();
  if ($pid === 0) {
    $app->stop();
  }
  return $response->withJson([$pid]);
});
