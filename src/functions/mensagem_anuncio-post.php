<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->post('/mensagem/anuncio', function (Request $request, Response $response, array $args) {
    global $sql_mensagens_pesquisador;

    $sessions = array();
    if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    //
    $form = $request->getParsedBody();
    $data['debug'][] = $form;
    $id_busca = $form['idBusca'];
    $dt_expiracao = $form['expiracao'];
    $tx_conteudo = $form['conteudo'];
    $tx_criterios = @$form['criterios'];
    $tx_extra = @$form['extra'];
    $id_usuario = 0;
    $tp_usuario = '';
    {
    	$uid = $request->getHeader('UID')[0];
        $data['debug'][] = array('uid' => $uid);
        $session = array();
        if (@array_key_exists($uid, $sessions)) {
            $session = $sessions[$uid];
            //echo "<pre>session ".var_export($session, true)."</pre>";
            $id_usuario = $session['id_usuario'];
            $tp_usuario = $session['tp_usuario'];
        }
        $data['debug'][] = array('id_usuario' => $id_usuario);
    }
    {
        $busca = array();
        if ($id_busca != null and $id_busca > 0) {
            $sql = "SELECT * FROM busca WHERE id LIKE :id_busca ";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_busca", $id_busca);
            $stmt->execute();
            $buscas = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $busca = $buscas[0];
        }
        $data['debug'][] = $busca;
        if (@empty($busca)) {
            $id_busca = 0;
        } else {
            $id_usuario = $busca['id_usuario'];
        }
        $data['debug'][] = $tx_criterios;
        if ($id_usuario > 0) {
            $sql = "SELECT * FROM usuario WHERE id = :id_usuario ";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->execute();
            $usuarios = $stmt->fetchAll(PDO::FETCH_OBJ);
            $tp_usuario = $usuarios[0]->tipo;
        }
        if ($id_busca > 0 and $id_usuario > 0) {
            $sql = "INSERT INTO mensagem (id_usuario, conteudo, extra) VALUES (:id_usuario, :conteudo, :extra);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":conteudo", $form['conteudo']);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->bindParam(":extra", $tx_extra);
            $stmt->execute();
            $id_mensagem = $db->lastInsertId();
            // */
            $sql = "INSERT INTO mensagem_anuncio (id_busca, id_mensagem, data_expiracao) VALUES (:id_busca, :id_mensagem, :data_expiracao);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":data_expiracao", $form['expiracao']);
            $stmt->bindParam(":id_busca", $id_busca);
            $stmt->bindParam(":id_mensagem", $id_mensagem);
            $stmt->execute();
        } else if (@empty($tx_criterios) == false) {
            $sql = "INSERT INTO mensagem (id_usuario, conteudo, extra) VALUES (:id_usuario, :conteudo, :extra);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":conteudo", $form['conteudo']);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->bindParam(":extra", $tx_extra);
            $stmt->execute();
            $id_mensagem = $db->lastInsertId();
            // */
            $sql = "INSERT INTO mensagem_anuncio (id_mensagem, data_expiracao) VALUES (:id_mensagem, :data_expiracao);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":data_expiracao", $form['expiracao']);
            $stmt->bindParam(":id_mensagem", $id_mensagem);
            $stmt->execute();
            $id_mensagem_anuncio = $db->lastInsertId();
            $data['debug'][] = array('id_mensagem_anuncio' => $id_mensagem_anuncio);

            $criterios = json_decode($tx_criterios);
            $data['debug'][] = array('criterios' => $criterios);

            foreach($criterios as $criterio) {
                $data['debug'][] = $criterio;
                $sql = "INSERT INTO resposta_buscada (id_mensagem_anuncio, id_questao, id_resposta, texto) VALUES (:id_mensagem_anuncio, :id_questao, :id_resposta, :texto);";
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id_questao", $criterio->id_questao);
                $stmt->bindParam(":id_resposta", $criterio->id_resposta);
                $stmt->bindParam(":texto", $criterio->texto);
                $stmt->bindParam(":id_mensagem_anuncio", $id_mensagem_anuncio);
                $stmt->execute();
                $id_resposta_buscada = $db->lastInsertId();
            }
        }
        if ($tp_usuario == "PESQUISADOR") {
            $db = getConnection();
            $stmt = $db->prepare($sql_mensagens_pesquisador);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->execute();
            $mensagens = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $data['mensagens'] = $mensagens;

            $data['debug'][] = $mensagens;
        }
        publish([
          [
            'topic' => "usuario-".$id_usuario."-inbox",
            'message' => "MESSAGES_UPDATE"
          ]
        ]);
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
