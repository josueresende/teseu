<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/pesquisador/questoes', function (Request $request, Response $response, array $args) {

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    $id_usuario = -1;
    $id_busca = -1;
    $id_versao = -1;
    try {
        $id_versao = $request->getQueryParam("idVersao");
    /*
        $usuario = $request->getQueryParam("usuario");
        $id_busca = $request->getQueryParam("idBusca");
        if (empty($usuario) == false) {
            $sql = "SELECT * FROM usuario WHERE nome LIKE :usuario ";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":usuario", $usuario);
            $stmt->execute();
            $usuarios = $stmt->fetchAll(PDO::FETCH_OBJ);
            $id_usuario = $usuarios[0]->id;
        }
    */
        if ($id_usuario == 0 and $id_busca == 0) {
            $data['error_code'] = 999;
            $data['error_description'] = "Consulta sem criterio.";
        } else {

            $sql =
                "SELECT DISTINCT " .
                "questao.id as questao_id, " .
                "questao.texto as questao_texto, " .
                "questao.texto_especifico as questao_especifico, " .
                "questao.is_criterio as questao_iscriterio, " .
                "questao.tag as questao_tag, " .
                "questao.config as questao_config, " .
                "questao.limite_marcadas as questao_limite_marcadas, " .
                "questao.id_versao as _versao, " .
                "'dummy' as dummy " .
                "FROM questao " .
                "WHERE id_versao > :id_versao "
            ;
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_versao", $id_versao);
            $stmt->execute();
            $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);
            $data['questoes'] = $resultado;
        }

//        $data['debug'] = array(@var_export($usuarios[0], true),@var_export($stmt, true),@var_export($form, true));

    } catch(PDOException $e) {
	    $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
