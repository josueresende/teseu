<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/mensagem/lista', function (Request $request, Response $response, array $args) {
    global $sql_mensagens_pesquisador,$sql_mensagens_voluntario;

    $id_usuario = $request->getAttribute('ID_USUARIO');
    $tp_usuario = $request->getAttribute('TP_USUARIO');

    error_log(var_export([
        __FILE__ => __LINE__,
        '$id_usuario' => $id_usuario,
        '$tp_usuario' => $tp_usuario,
    ], true));

    if ($request->getAttribute('VALIDATION') == false) return $response->withStatus(403);

    // $sessions = array();
    // if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    //
    //$usuario = $request->getQueryParam("usuario");
    $id_mensagem = $request->getQueryParam("idMensagem");
    // $id_usuario = 0;
    // $tp_usuario = '';

    try {
        if (@empty($id_mensagem)) $id_mensagem = 0;
        $data['debug'][] = array('id_mensagem' => $id_mensagem);
        $db = getConnection();
        // {
        //     $uid = $request->getHeader('UID')[0];
        //     $data['debug'][] = array('uid' => $uid);
        //     $session = array();
        //     if (@array_key_exists($uid, $sessions)) {
        //         $session = $sessions[$uid];
        //         //echo "<pre>session ".var_export($session, true)."</pre>";
        //         $id_usuario = $session['id_usuario'];
        //         $tp_usuario = $session['tp_usuario'];
        //     }
        //     $data['debug'][] = array('id_usuario' => $id_usuario, 'tp_usuario' => $tp_usuario);
        // }
        /*
        if (empty($usuario) == false) {
            $sql = "SELECT * FROM usuario WHERE nome LIKE :usuario ";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":usuario", $usuario);
            $stmt->execute();
            $usuarios = $stmt->fetchAll(PDO::FETCH_OBJ);
            $id_usuario = $usuarios[0]->id;
            $tp_usuario = $usuarios[0]->tipo;
        }
        // */
        if ($id_usuario == 0) {
            $data['error_code'] = 999;
            $data['error_description'] = "Consulta sem criterio.";
        } else
        if ($tp_usuario == "PESQUISADOR") {
            if ($id_mensagem > 0) {
                $sql_mensagens_pesquisador = str_replace("AND 1=1", " AND mensagem.id > :id_mensagem ", $sql_mensagens_pesquisador);
            }
            $stmt = $db->prepare($sql_mensagens_pesquisador);
            $stmt->bindParam(":id_usuario", $id_usuario);
            if ($id_mensagem > 0) {
                $stmt->bindParam(":id_mensagem", $id_mensagem);
            }
            $stmt->execute();
            $mensagens = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $data['mensagens'] = $mensagens;

            // criterios atrelados a anuncios
            $sql =
                "SELECT DISTINCT " .
                "resposta_buscada.id as buscada_id, " .
                "resposta_buscada.id_questao as questao_id, " .
                "resposta_buscada.id_resposta as resposta_id, " .
                "resposta_buscada.id_mensagem_anuncio as anuncio_id, " .
                "resposta_buscada.texto as buscada_texto, " .
                "resposta_buscada._deleted as buscada_deleted, " .
                "resposta.conteudo as resposta_conteudo, " .
                "resposta.texto as resposta_texto, " .
                "'dummy' as dummy " .
                "FROM resposta_buscada  " .
                "INNER JOIN resposta ON resposta.id = resposta_buscada.id_resposta " .
                "INNER JOIN mensagem_anuncio ON mensagem_anuncio._deleted = 'N' AND mensagem_anuncio.id = resposta_buscada.id_mensagem_anuncio " .
                "INNER JOIN mensagem ON mensagem.id = mensagem_anuncio.id_mensagem " .
                "WHERE mensagem.id_usuario = :id_usuario " .
                ""
            ;
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->execute();
            $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);
            $data['respostas'] = $resultado;

            $data['debug'][] = $mensagens;
        } else if ($tp_usuario == "VOLUNTARIO") {
            if ($id_mensagem > 0) {
                $sql_mensagens_voluntario = str_replace("AND 1=1", " AND mensagem.id > :id_mensagem ", $sql_mensagens_voluntario);
            }
            $stmt = $db->prepare($sql_mensagens_voluntario);
            $stmt->bindParam(":id_usuario", $id_usuario);
            if ($id_mensagem > 0) {
                $stmt->bindParam(":id_mensagem", $id_mensagem);
            }
            $stmt->execute();
            $mensagens = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $data['mensagens'] = $mensagens;
            $data['debug'][] = $mensagens;
        }
    } catch(PDOException $e) {
        $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
        $data['debug'][] = array('error_description' => $e->getMessage());
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
