<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/voluntario/formularios', function (Request $request, Response $response, array $args) {
    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    $usuario = $request->getQueryParam("usuario");
    try {
        $sql =
            "SELECT DISTINCT " .
            "formulario.id as formulario_id, " .
            "formulario.titulo as formulario_titulo, " .
            "formulario.resposta_texto_livre as formulario_texto," .
            "'dummy' as dummy " .
            "FROM formulario " .
            "INNER JOIN questao_formulario ON questao_formulario.id_formulario = formulario.id " .
            "INNER JOIN questao ON questao.id = questao_formulario.id_questao " .
            "LEFT JOIN resposta ON resposta.id_questao = questao.id " .
            "LEFT JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id ".
                      "AND resposta_marcada.id_usuario IN (SELECT id FROM usuario WHERE usuario.nome = :usuario) " .
            "WHERE formulario.id_resposta_referencia IS NULL " .
            "OR formulario.id_resposta_referencia IN ".
            "( SELECT id_resposta FROM resposta_marcada INNER JOIN usuario ON usuario.id = resposta_marcada.id_usuario WHERE usuario.nome = :usuario ) ".
            "ORDER BY formulario.id "
        ;
        $db = getConnection();
        $stmt = $db->prepare($sql);

        $stmt->bindParam(":usuario", $usuario);

        $stmt->execute();

        $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);

        $data['formularios'] = $resultado;

    } catch(PDOException $e) {
	    $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
