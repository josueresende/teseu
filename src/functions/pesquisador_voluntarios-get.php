<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/pesquisador/voluntarios', function (Request $request, Response $response, array $args) {

    $id_usuario = $request->getAttribute('ID_USUARIO');
    $tp_usuario = $request->getAttribute('TP_USUARIO');

    error_log(var_export([
        __FILE__ => __LINE__,
        '$id_usuario' => $id_usuario,
        '$tp_usuario' => $tp_usuario,
    ], true));

    if ($request->getAttribute('VALIDATION') == false) return $response->withStatus(403);

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    // $id_usuario = 0;
    $id_busca = 0;

    try {
        $db = getConnection();
        $id_busca = $request->getQueryParam("idBusca");
        // $usuario = $request->getQueryParam("usuario");

        // if (empty($usuario) == false) {
        //     $sql = "SELECT * FROM usuario WHERE nome LIKE :usuario ";
        //     $db = getConnection();
        //     $stmt = $db->prepare($sql);
        //     $stmt->bindParam(":usuario", $usuario);
        //     $stmt->execute();
        //     $usuarios = $stmt->fetchAll(PDO::FETCH_OBJ);
        //     $id_usuario = $usuarios[0]->id;
        // }

        $data['debug'][] =['$id_busca' => $id_busca, '$id_usuario' => $id_usuario];

        if ($id_busca == 0 or $id_usuario == 0) {
            $data['error_code'] = 999;
            $data['error_description'] = "Consulta sem criterio.";
        } else {
            $sql = "
SELECT DISTINCT
usuario.id AS voluntario_id,
usuario.nome AS voluntario_nome,
usuario.loc_latitude AS voluntario_latitude,
usuario.loc_longitude AS voluntario_longitude,
pesquisador.id AS pesquisador_id,
pesquisador.loc_latitude AS pesquisador_latitude,
pesquisador.loc_longitude AS pesquisador_longitude,
(
6371  *
    acos(
        cos(radians(pesquisador.loc_latitude)) *
        cos(radians(usuario.loc_latitude)) *
        cos(radians(pesquisador.loc_longitude) - radians(usuario.loc_longitude)) +
        sin(radians(pesquisador.loc_latitude)) *
        sin(radians(usuario.loc_latitude))
    )
) AS distancia,
'dummy' AS dummy
FROM usuario
,(
    SELECT id, loc_latitude, loc_longitude
    FROM usuario
    WHERE id = :id_pesquisador
) AS pesquisador
WHERE 1=1
AND usuario.id IN (
  SELECT id_usuario
  FROM (
      SELECT resposta_marcada.id_usuario,count(resposta.id) AS checked
      FROM resposta_marcada
      INNER JOIN resposta ON resposta.id = resposta_marcada.id_resposta AND resposta.conteudo = 'MARCACAO'
      INNER JOIN resposta_buscada ON resposta_buscada.id_resposta = resposta_marcada.id_resposta
      WHERE resposta_buscada._deleted = 'N' AND resposta_buscada.id_busca = :id_busca
      GROUP BY resposta_marcada.id_usuario
  UNION ALL
      SELECT resposta_marcada.id_usuario, 1 AS checked
      FROM resposta
      INNER JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id
      INNER JOIN resposta_buscada ON resposta_buscada.id_resposta = resposta.id
      WHERE resposta_buscada._deleted = 'N' AND resposta_buscada.id_busca = :id_busca
      AND resposta.conteudo = 'DATA'
      AND resposta_marcada.texto IS NOT NULL
      AND TIMESTAMPDIFF(YEAR,STR_TO_DATE(resposta_marcada.texto,'%Y-%m-%d'),CURDATE()) BETWEEN JSON_EXTRACT(resposta_buscada.texto, '$.min') AND JSON_EXTRACT(resposta_buscada.texto, '$.max')
  UNION ALL
      SELECT resposta_marcada.id_usuario, 1 AS checked
      FROM resposta
      INNER JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id
      INNER JOIN resposta_buscada ON resposta_buscada.id_questao = resposta.id_questao
      WHERE resposta_buscada._deleted = 'N' AND resposta_buscada.id_busca = :id_busca
      AND resposta.conteudo = 'DURACAO_MESES'
      AND resposta_marcada.texto IS NOT NULL
      AND TIMESTAMPDIFF(MONTH,STR_TO_DATE(resposta_marcada.texto,'%Y-%m-%d'),CURDATE()) BETWEEN JSON_EXTRACT(resposta_buscada.texto, '$.min') AND JSON_EXTRACT(resposta_buscada.texto, '$.max')
  ) AS SOMA
  GROUP BY id_usuario
  HAVING SUM(checked) = ( SELECT count(id) FROM resposta_buscada  WHERE resposta_buscada._deleted = 'N' AND resposta_buscada.id_busca = :id_busca)
)
            "
            ;
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_busca", $id_busca);
            $stmt->bindParam(":id_pesquisador", $id_usuario);
            $stmt->execute();

            $voluntarios = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // TRAZER as respostas respondidas EM 3 DIAS
            foreach ($voluntarios as $voluntario) {
                {
                    $data['debug'][] = ['voluntario' => $voluntario];
                    $sql =
                        "SELECT DISTINCT " .
                        "formulario.id as formulario_id, " .
                        "formulario.id_resposta_referencia as formulario_id_resposta_referencia, " .
                        "questao.id as questao_id, " .
                        "questao.tag as questao_tag, " .
                        "resposta.id as resposta_id, " .
                        "resposta.conteudo as resposta_conteudo, " .
                        "resposta.texto as resposta_texto, " .
                        "resposta_marcada.id as marcada_id, " .
                        "resposta_marcada.texto as marcada_texto, " .
                        "'dummy' as dummy " .
                        "FROM formulario  " .
                        "INNER JOIN questao_formulario ON questao_formulario.id_formulario = formulario.id " .
                        "INNER JOIN questao ON questao.id = questao_formulario.id_questao " .
                        "LEFT JOIN resposta ON resposta.id_questao = questao.id " .
                        "INNER JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id ".
                                  "AND resposta_marcada.id_usuario = :id_usuario " .
                        "WHERE formulario.id_resposta_referencia IS NULL " .
                        "OR formulario.id_resposta_referencia IN ".
                        "( SELECT id_resposta FROM resposta_marcada WHERE resposta_marcada.id_usuario = :id_usuario ) ".
                        "ORDER BY formulario.id,questao.id,resposta.id "
                    ;
                    $db = getConnection();
                    $stmt = $db->prepare($sql);

                    $stmt->bindParam(":id_usuario", $voluntario['voluntario_id']);

                    $stmt->execute();

                    $voluntario['respostas'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
                }
                $data['voluntarios'][] = $voluntario;
            }
        }

        // $data['debug'] = array(@var_export($usuarios[0], true),@var_export($stmt, true),@var_export($form, true));

    } catch(PDOException $e) {
	    $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
