<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->post('/usuario/registrar', function (Request $request, Response $response, array $args) {
    $sessions = array();
    if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));
    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    $form = $request->getParsedBody();
    try {
        // $sessions = array();
        // if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));

        $uid = $request->hasHeader('UID') ? $request->getHeader('UID')[0] : $request->getQueryParam("UID");
        $usr = $form["username"];
        $pwd = $form["password"];

        if (!empty($uid) and !empty($usr) and !empty(pwd)) {
            $form['username'] = local_decrypt($uid, $usr);
            $form['password'] = local_decrypt($uid, $pwd);
        }
    } catch(Exception $e) {
        $data['decrypt'] = $e->getMessage();
    }
    try {
        $db = getConnection();

        $data['debug'][] = array(@var_export($form, true));
        if ($data['error_code'] == 0 and !empty($form['username']) and !empty($form['password'])) {
            $sql = "SELECT * FROM usuario WHERE login LIKE :login ";
            $stmt = $db->prepare($sql);

            $stmt->bindParam(":login", md5(sha1($form['username'])));

            $stmt->execute();

            $usuarios = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $data['debug'][] = array(@var_export($usuarios, true),@var_export($stmt, true),@var_export($form, true));

            error_log(var_export($usuarios, true));

            $usuario = [];
            $id_usuario = -1;
            if (count($usuarios) > 0) {
              $usuario = $usuarios[0];
            }

            if ($usuario['_enabled'] == 'S') {
                $data['error_code'] = 999;
                $data['error_description'] = "Esse usuário já está registrado.";
                $id_usuario = $usuario['id'];
            } else if ($usuario['_enabled'] == 'N') {
                $id_usuario = $usuario['id'];
                $sql = "UPDATE usuario SET tipo = :tipo, senha = :senha WHERE id = :id;";
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id", $id_usuario);
                $stmt->bindParam(":tipo", $form['tipo']);
                $stmt->bindParam(":senha", md5(md5($form['password'])));
                $stmt->execute();
            } else if (filter_var($form['username'], FILTER_VALIDATE_EMAIL)) {
                $anonymiz_nome = "";
                // usuario
                $sql = "INSERT INTO usuario (tipo, login, nome, senha) VALUES (:tipo, :login, :nome, :senha);";
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":tipo", $form['tipo']);
                $stmt->bindParam(":nome", $form['username']);
                $stmt->bindParam(":login", md5(sha1($form['username'])));
                $stmt->bindParam(":senha", md5(md5($form['password'])));
                $stmt->execute();
                $id_usuario = $db->lastInsertId();
            } else {
                $data['error_code'] = 999;
                $data['error_description'] = "Utilize um e-mail válido.";
            }
            if ($id_usuario > 0) {
                // invalidar token anteriores
                $sql =  "UPDATE token SET token.data_delecao = now() WHERE token.acao = 'USER_REGISTER' and token.id_usuario = :id_usuario ";
                $db = getConnection();
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id_usuario", $id_usuario);
                $stmt->execute();
                // criar token de reset com validade
                $hash = sha1(md5(uniqid(rand(), true)));
                $expiracao = new DateTime('now');
                $expiracao = $expiracao->modify('+1 day');
                $dataDeDelecao = $expiracao->format("Y-m-d H:i:s");
                $sql = "INSERT INTO token (id_usuario, data_delecao, acao, hash) VALUES (:id_usuario, :data_delecao, 'USER_REGISTER', :hash);";
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id_usuario", $id_usuario);
                $stmt->bindParam(":data_delecao", $dataDeDelecao);
                $stmt->bindParam(":hash", $hash);
                $stmt->execute();
                // enviar e-mail
                $link = "http://".$_SERVER['HTTP_HOST'].'/token/usuario/registrar/'.$hash;
                mailer($form['username'],
                      "Registro na Plataforma",
                      "<p>Ol&aacute;,</p>".
                      "<p style='padding-left: 40px;'>Obrigado por ter se inscrito em nossa plataforma como {$form['tipo']}.</p>".
                      "<p style='padding-left: 40px;'>Desejamos que a utilize da melhor forma poss&iacute;vel.</p>".
                      "<p style='padding-left: 40px;'>Clique no <a href='$link'>link</a> para completar esta a&ccedil;&atilde;o.</p>".
                      "<p>Atenciosamente</p>".
                      "<p>Equipe de Desenvolvimento</p>");
            }
        } else {
            $data['error_code'] = 999;
            $data['error_description'] = "Sem dados para registro";
        }
// */
    } catch(Exception $e) {
	      $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
// */
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
