<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;

$app->post('/localizacao', function (Request $request, Response $response, array $args) {
    $form = $request->getParsedBody();
    $id = -1;
    try {
        $google = explode('/', (@$form['google'] ?? ''));
        $key = array_search('place', $google);
        if ($key) {
          $gps_location = explode(',', strtr((@$google[$key + 2] ?? ''), ['@' => '']));
          $data = [
              'tipo' => 'CENTRO_DE_PESQUISA',
              'nome' => urldecode(@$google[$key + 1] ?? ''),
              'latitude' => (@$gps_location[0] ?? null),
              'longitude' => (@$gps_location[1] ?? null),
          ];
          // localizacao
          $db = getConnection();
          $sql = "INSERT INTO localizacao (tipo, endereco, nome, loc_latitude, loc_longitude) VALUES (:tipo, '', :nome, :latitude, :longitude);";
          $stmt = $db->prepare($sql);
          $stmt->bindParam(":tipo", $data['tipo']);
          $stmt->bindParam(":nome", $data['nome']);
          $stmt->bindParam(":latitude", $data['latitude']);
          $stmt->bindParam(":longitude", $data['longitude']);
          $stmt->execute();
          $id = $db->lastInsertId();
          $form['google'] = null;
        }
    } catch (\Exception $e) {
        $form['exception'] = $e;
    }

    __html([
      'head' => ['title' => 'Localização de Centros'],
      'body' => [
        'form' => [
          'action' => '/localizacao',
          'input' => [
              [
                'name' => 'google',
                'placeholder' => 'URL do Google',
                'value' => (@$form['google'] ?? ''),
                'text' => ($id < 0 ? 'Erro ao inserir' : $data['nome'].' inserido com sucesso.'),
                'invalid' => is_null($form['exception']) ? false : ('<pre>'.var_export([@$form['exception']], true).'</pre>'),
              ]
          ]
        ]
      ]
    ]);
});
$app->get('/localizacao', function (Request $request, Response $response, array $args) {
    __html([
      'head' => ['title' => 'Localização de Centros'],
      'body' => [
        'form' => [
          'action' => '/localizacao',
          'input' => [
              ['name' => 'google', 'placeholder' => 'URL do Google']
          ]
        ]
      ]
    ]);
});

function formLocalizacaoPost() {
}

function formLocalizacaoGet() {
}

// $app->post('/token/senha/reset/{hash}', function (Request $request, Response $response, array $args) {
//     $form = $request->getParsedBody();
//     try {
//         $sql =  "SELECT token.id_usuario, token.acao, token.hash " .
//                 "FROM token " .
//                 // "INNER JOIN usuario ON usuario.id = token.id_usuario " .
//                 "WHERE token.acao = 'PASSWORD_RESET' AND token.hash = :hash " .
//                 "AND now() < token.data_delecao ";
//         $db = getConnection();
//         $stmt = $db->prepare($sql);
//         $stmt->bindParam(":hash", $args['hash']);
//         $stmt->execute();
//         $tokens = $stmt->fetchAll(PDO::FETCH_ASSOC);
//         if (count($tokens) == 0) return $response->withStatus(403);
//         $token = $tokens[0];
//         $data['debug'][] = ['$tokens' => $tokens, '$token' => $token];
//     } catch (Exception $e) {
//         //
//     }
//
//     $parametros = ['hash' => $args['hash'],'email' => $form['email']];
//
//     error_log(var_export($parametros, true));
//
//     if ($form['password'] != $form['password-confirm']) $parametros['password-invalido'] = true;
//
//     if (filter_var($form['email'], FILTER_VALIDATE_EMAIL) != $form['email']) $parametros['email-invalido'] = true;
//
//     error_log(var_export($parametros, true));
//
//     if (array_key_exists('email-invalido', $parametros) || array_key_exists('password-invalido', $parametros)) {
//         formPasswordReset($parametros);
//         return $response->withStatus(200);
//     }
//
//     try {
//         $sql =  "UPDATE token SET token.data_delecao = now() WHERE token.hash = :hash ";
//         $db = getConnection();
//         $stmt = $db->prepare($sql);
//         $stmt->bindParam(":hash", $token['hash']);
//         $stmt->execute();
//
//         $senha = md5(md5($form['password']));
//
//         $sql =  "UPDATE usuario SET usuario._enabled = 'S', usuario.senha = :senha WHERE usuario.id = :id_usuario ";
//         $db = getConnection();
//         $stmt = $db->prepare($sql);
//         $stmt->bindParam(":senha", $senha);
//         $stmt->bindParam(":id_usuario", $token['id_usuario']);
//         $stmt->execute();
//
//         formPasswordResetOk();
//
//     } catch (Exception $e) {
//         //
//     }
//
//     return $response->withStatus(200);
// });
//

// $app->get('/token/senha/reset/{hash}', function (Request $request, Response $response, array $args) {
//     $data['debug'][] = ['$args' => $args];
//     try {
//       $sql =  "SELECT token.id_usuario, token.acao " .
//               "FROM token " .
//               "WHERE token.acao = 'PASSWORD_RESET' AND token.hash = :hash " .
//               "AND now() < token.data_delecao ";
//       $db = getConnection();
//       $stmt = $db->prepare($sql);
//       $stmt->bindParam(":hash", $args['hash']);
//       $stmt->execute();
//       $tokens = $stmt->fetchAll(PDO::FETCH_ASSOC);
//       if (count($tokens) == 0) return $response->withStatus(403);
//       $token = $tokens[0];
//       $data['debug'][] = ['$tokens' => $tokens, '$token' => $token];
//       formPasswordReset($args);
//     } catch (Exception $e) {
//
//     }
//     return $response->withStatus(200);
// });
