<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/voluntario/respostas', function (Request $request, Response $response, array $args) {

    $id_usuario = $request->getAttribute('ID_USUARIO');
    $tp_usuario = $request->getAttribute('TP_USUARIO');

    error_log(var_export([
        __FILE__ => __LINE__,
        '$id_usuario' => $id_usuario,
        '$tp_usuario' => $tp_usuario,
    ], true));

    if ($request->getAttribute('VALIDATION') == false) return $response->withStatus(403);

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    // $usuario = $request->getQueryParam("usuario");
    try {
        $sql =
            "SELECT DISTINCT " .
            "formulario.id as formulario_id, " .
            "questao.id as questao_id, " .
            "questao.tag as questao_tag, " .
            "resposta.id as resposta_id, " .
            "resposta.conteudo as resposta_conteudo, " .
            "resposta.texto as resposta_texto, " .
            "resposta_marcada.id as marcada_id, " .
            "resposta_marcada.texto as marcada_texto, " .
            "questao_formulario.ordem_exibicao as ordem_exibicao, ".
            "'dummy' as dummy " .
            "FROM formulario  " .
            "INNER JOIN questao_formulario ON questao_formulario.id_formulario = formulario.id " .
            "INNER JOIN questao ON questao.id = questao_formulario.id_questao " .
            "LEFT JOIN resposta ON resposta.id_questao = questao.id " .
            "LEFT JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id ".
                      "AND resposta_marcada.id_usuario = :id_usuario " .
            // "WHERE formulario.id_resposta_referencia IS NULL " .
            // "OR formulario.id_resposta_referencia IN ".
            // "( SELECT id_resposta FROM resposta_marcada INNER JOIN usuario ON usuario.id = resposta_marcada.id_usuario WHERE usuario.nome = :usuario ) ".
            // "ORDER BY formulario.id,questao.id,resposta.id "
            "ORDER BY questao_formulario.ordem_exibicao ".
            ""
        ;
        $db = getConnection();
        $stmt = $db->prepare($sql);

        $stmt->bindParam(":id_usuario", $id_usuario);

        $stmt->execute();

        $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);

        $data['respostas'] = $resultado;

        $data['sql'] = $sql;

    } catch(PDOException $e) {
        $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
