<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$_session_open = [];
$app->post('/session/open', function (Request $request, Response $response, array $args) {
    global $_session_open;
    $sessions = array();
    try {
        if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));
        error_log(var_export(['$sessions keys', array_keys($sessions)], true));
        $data = array(
          'error_code' => 0,
          'error_description' => 'SUCCESS',
          // 'debug' => [['$sessions' => $sessions]],
        );
        $body = $request->getParsedBody();
        // $data['debug'][] = ['$body' => $body];
        if ($request->hasHeader('UID') and array_key_exists('publickey', $body)) {
            $uid = $request->getHeader('UID')[0];
            $data['uid'] = $uid;
            // procura sessao
    	      $session = array();
    	      if (@array_key_exists($uid, $sessions) == false) {
                $keyPair = (new RSA())->createKey(768);
                ($rsaPrivateKey = new RSA())->loadKey($keyPair['privatekey']);
                ($rsaPublicKey = new RSA())->loadKey($keyPair['publickey']);
                $rsaPrivateKey->setSignatureMode(RSA::SIGNATURE_PKCS1);
                $rsaPublicKey->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
                $session['local.private'] = $rsaPrivateKey->getPrivateKey();
                $session['local.public'] = $rsaPublicKey->getPublicKey();
                $session['remote.public'] = $body['publickey'];

                $sessions[$uid] = $session;

                @file_put_contents('sessions', serialize($sessions));
            } else {
                $session = $sessions[$uid];
            }
            error_log(var_export(['$session keys', array_keys($session)], true));
            if (@array_key_exists('local.public', $session)) {
                $data['pubk'] = $session['local.public'];
            }
        }
    } catch(Exception $e) {
        // $data['debug']['Exception $e'] = $e->getMessage();
        error_log(var_export(['Exception $e', $e], true));
    }
    error_log(var_export(['$data', $data], true));
    // $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
