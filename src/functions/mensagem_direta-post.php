<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->post('/mensagem/direta', function (Request $request, Response $response, array $args) {
    $sessions = array();
    if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );

	$uid = $request->getHeader('UID')[0];
	$man = $request->hasHeader('Manufacturer') ? $request->getHeader('Manufacturer')[0] : '';
	$mod = $request->hasHeader('Model') ? $request->getHeader('Model')[0] : '';

    $id_usuario = 0;
    $tp_usuario = '';
    $id_leitor = 0;
    $tp_leitor = '';
    {
        $session = array();
        if (@array_key_exists($uid, $sessions)) {
            $session = $sessions[$uid];
            $id_usuario = $session['id_usuario'];
            $tp_usuario = $session['tp_usuario'];
        }
        $data['debug'][] = array('id_usuario' => $id_usuario, 'tp_usuario' => $tp_usuario);
    }

    if ($id_usuario == 0) {
        $data = array(
            'error_code' => 503,
            'error_description' => 'CALL ME ISHMAEL ...',
        );
        $data['debug'] = base64_encode(@var_export($data['debug'], true));
        return $response->withJson($data);
    }

    $form = $request->getParsedBody();
    $tx_extra = @$form['extra'];

    try {
        $usuario = $form['usuario'];
        $conteudo = $form['conteudo'];
        $data['debug'][] = array ('usuario' => $usuario, 'conteudo' => $conteudo);

        if (empty($usuario) == false) {
            $sql = "SELECT * FROM usuario WHERE nome LIKE :usuario ";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":usuario", $usuario);
            $stmt->execute();
            $usuarios = $stmt->fetchAll(PDO::FETCH_OBJ);
            $id_leitor = $usuarios[0]->id;
            $tp_leitor = $usuarios[0]->tipo;
        }
        if ($id_leitor == 0) {
            $data = array(
                'error_code' => 504,
                'error_description' => 'CAN YOU HEAR ME ?',
            );
        } else {
            $sql = "INSERT INTO mensagem (id_usuario, conteudo, extra) VALUES (:id_usuario, :conteudo, :extra);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":conteudo", $form['conteudo']);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->bindParam(":extra", $tx_extra);
            $stmt->execute();
            $id_mensagem = $db->lastInsertId();
            // */
            $sql = "INSERT INTO mensagem_direta (id_usuario, id_mensagem) VALUES (:id_usuario, :id_mensagem);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $id_leitor);
            $stmt->bindParam(":id_mensagem", $id_mensagem);
            $stmt->execute();

            publish([
              [
                'topic' => "usuario-".$id_leitor."-inbox",
                'message' => "MESSAGES_UPDATE"
              ]
            ]);
            publish([
              [
                'topic' => "usuario-".$id_usuario."-inbox",
                'message' => "MESSAGES_UPDATE"
              ]
            ]);

        }

    } catch(PDOException $e) {
	    $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
        $data['debug'][] = array('error_description' => $e->getMessage());
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
