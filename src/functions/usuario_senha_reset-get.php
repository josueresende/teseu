<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/usuario/senha/reset', function (Request $request, Response $response, array $args) {
    global $sql_mensagens_pesquisador, $_local_decrypt;

    // if ($request->getAttribute('VALIDATION') == true) return $response->withStatus(407);

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );

    $uid = null;

    $sessions = array();
    $session = array();

    $usuario = [];

    $em_usuario = $request->getQueryParam('email');
    $id_usuario = 0;
    $tp_usuario = '';

    {
        $sql =  "SELECT id, tipo " .
                "FROM usuario " .
                "WHERE login LIKE :login ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":login", md5(sha1($em_usuario)));
        $stmt->execute();
        $usuarios = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($usuarios) > 0) {
            $usuario = @$usuarios[0];
            $id_usuario = $usuario['id'];
            $tp_usuario = $usuario['tipo'];
        }
    }

    if ($id_usuario == 0 || $tp_usuario == '') {
        return $response->withStatus(503);
    }

    try {
        // invalidar token anteriores
        $sql =  "UPDATE token SET token.data_delecao = now() WHERE token.acao = 'PASSWORD_RESET' and token.id_usuario = :id_usuario ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":id_usuario", $id_usuario);
        $stmt->execute();
        // criar token de reset com validade
        $hash = sha1(md5(uniqid(rand(), true)));
        $expiracao = new DateTime('now');
        $expiracao = $expiracao->modify('+1 day');
        $dataDeDelecao = $expiracao->format("Y-m-d H:i:s");
        $sql = "INSERT INTO token (id_usuario, data_delecao, acao, hash) VALUES (:id_usuario, :data_delecao, 'PASSWORD_RESET', :hash);";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":id_usuario", $id_usuario);
        $stmt->bindParam(":data_delecao", $dataDeDelecao);
        $stmt->bindParam(":hash", $hash);
        $stmt->execute();
        // enviar email com o token
        {
            $link = "http://".$_SERVER['HTTP_HOST'].'/token/senha/reset/'.$hash;

            // $sql =  "SELECT nome as email " .
            //         "FROM usuario " .
            //         "WHERE id LIKE :id ";
            // $db = getConnection();
            // $stmt = $db->prepare($sql);
            // $stmt->bindParam(":id", $id_usuario);
            // $stmt->execute();
            // $usuarios = $stmt->fetchAll(PDO::FETCH_ASSOC);
            // $usuario = @$usuarios[0];
            $mail = [
              'to' => $em_usuario,
              'subject' => "Solicitação de nova senha em ".date('d/m/Y'),
              'body' => "<p>Ol&aacute;,</p>".
                  "<p style='padding-left: 40px;'>Voc&ecirc; solicitou recentemente uma nova senha por ter esquecido.</p>".
                  "<p style='padding-left: 40px;'>Clique no <a href='$link'>link</a> para completar esta a&ccedil;&atilde;o.</p>".
                  "<p>Atenciosamente</p>".
                  "<p>Equipe de Desenvolvimento</p>"
            ];
            $data['debug'][] = $mail;

            mailer($mail['to'], $mail['subject'], $mail['body']);
        }

    } catch(Exception $e) {
        $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    error_log(var_export($data, true));
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
/*
CREATE TABLE `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `data_criacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_delecao` datetime DEFAULT NULL,
  `acao` longtext,
  `hash` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=latin1
 */
