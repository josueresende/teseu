<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/pesquisador/respostas', function (Request $request, Response $response, array $args) {
  
    // $id_usuario = $request->getAttribute('ID_USUARIO');
    // $tp_usuario = $request->getAttribute('TP_USUARIO');
    //
    // error_log(var_export([
    //     __FILE__ => __LINE__,
    //     '$id_usuario' => $id_usuario,
    //     '$tp_usuario' => $tp_usuario,
    // ], true));
    //
    // if ($request->getAttribute('VALIDATION') == false) return $response->withStatus(403);

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    // $usuario = $request->getQueryParam("usuario");

    $id_versao = -1;

    try {
        $id_versao = $request->getQueryParam("idVersao");
    /*
        $sql = "SELECT * FROM usuario WHERE nome LIKE :usuario ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":usuario", $usuario);
        $stmt->execute();

        $usuarios = $stmt->fetchAll(PDO::FETCH_OBJ);

        if ($usuarios == null) {
            $data['error_code'] = 999;
            $data['error_description'] = "Usuário inexistente.";
        } else {
    */
            $sql =
                "SELECT DISTINCT " .
                "questao.id as questao_id, " .
                "questao.texto as questao_texto, " .
                "questao.limite_marcadas as questao_limite_marcadas, " .
                "resposta.id as resposta_id, " .
                "resposta.conteudo as resposta_conteudo, " .
                "resposta.texto as resposta_texto, " .
                "resposta.tag as resposta_tag, " .
                "resposta.id_versao as _versao, " .
                "'dummy' as dummy " .
                "FROM resposta " .
                "LEFT JOIN questao ON questao.id = resposta.id_questao " .
                "WHERE resposta.id_versao > :id_versao "
            ;
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_versao", $id_versao);
            $stmt->execute();
            $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);
            $data['respostas'] = $resultado;
    /*
        }
    */

//        $data['debug'] = array(@var_export($usuarios[0], true),@var_export($stmt, true),@var_export($form, true));

    } catch(PDOException $e) {
	      $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
