<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

function formUserRemoverOk() { ?>
<!DOCTYPE html>
<html>
 <head>
   <meta charset="UTF-8">
   <title></title>
   <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
   <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link href="/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
 </head>
 <body class="login-page">
   <div class="login-box">
     <div class="login-logo">
       <!-- Login  -->
     </div><!-- /.login-logo -->
     <div class="login-box-body">
     	<img src="/images/revolution_logo.png" width="320px;"><br/><br/>
       <div class="row">
           <div class="col-md-12">
           </div>
       </div>
       <div class="row">
           <div class="col-md-12">
             Seu perfil foi removido completamente.
           </div>
       </div>
     </div><!-- /.login-box-body -->
   </div><!-- /.login-box -->
   <script src="/js/jQuery-2.1.4.min.js"></script>
   <script src="/js/bootstrap.min.js" type="text/javascript"></script>
 </body>
</html>
<?php }

function formUserRemover($config) { ?>
<!DOCTYPE html>
<html>
 <head>
   <meta charset="UTF-8">
   <title></title>
   <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
   <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link href="/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
 </head>
 <body class="login-page">
   <div class="login-box">
     <div class="login-logo">
       <!-- Login  -->
     </div><!-- /.login-logo -->
     <div class="login-box-body">
     	<img src="/images/revolution_logo.png" width="320px;"><br/><br/>
       <div class="row">
           <div class="col-md-12">
           </div>
       </div>
       <form action="/token/usuario/remover/<?php echo $config['hash']; ?>" method="post">
         <div class="form-group has-feedback">
           Voc&ecirc; ser&aacute; removido completamente da plataforma.
         </div>
         <div class="form-group has-feedback">
           <?php if ($config['password-invalido']) : ?>
            <div class="input row-input with-error has-error text-red">Senha incorreta, tente novamente.</div>
           <?php endif; ?>
           <input type="password" class="form-control" placeholder="Password" name="password" required />
           <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         </div>
         <div class="row">
           <div class="col-xs-8">
             <!-- <div class="checkbox icheck">
               <label>
                 <input type="checkbox"> Remember Me
               </label>
             </div>  -->
           </div><!-- /.col -->
           <div class="col-xs-8">
             <input type="submit" class="btn btn-primary btn-block btn-flat" value="REMOVER USUARIO" />
           </div><!-- /.col -->
         </div>
       </form>
     </div><!-- /.login-box-body -->
   </div><!-- /.login-box -->
   <script src="/js/jQuery-2.1.4.min.js"></script>
   <script src="/js/bootstrap.min.js" type="text/javascript"></script>
 </body>
</html>
<?php }

$app->post('/token/usuario/remover/{hash}', function (Request $request, Response $response, array $args) {
    $token['id_usuario'] = -1;
    $form = $request->getParsedBody();
    try {
        $sql =
        " SELECT token.id_usuario, usuario.senha, token.acao, token.hash
          FROM token
          INNER JOIN usuario ON usuario.id = token.id_usuario
          WHERE token.acao = 'USER_COMPLETE_REMOVE' AND token.hash = :hash AND now() < token.data_delecao ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":hash", $args['hash']);
        $stmt->execute();
        $tokens = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($tokens) == 0) return $response->withStatus(403);
        $token = $tokens[0];
        $data['debug'][] = ['$tokens' => $tokens, '$token' => $token];
    } catch (Exception $e) {
        //
    }

    $parametros = ['hash' => $args['hash'],'email' => $form['email']];

    // error_log(var_export([
    //     'password-from-page' => $form['password'],
    //     'password' => md5(md5($form['password'])),
    //     'password-from-bd' => $token['senha']
    // ], true));

    // if ($form['password'] != $form['password-confirm']) $parametros['password-invalido'] = true;

    // if (filter_var($form['email'], FILTER_VALIDATE_EMAIL) != $form['email']) $parametros['email-invalido'] = true;
    if (md5(md5($form['password'])) != $token['senha']) {
        $parametros['password-invalido'] = true;
        formUserRemover($parametros);
        return $response->withStatus(200);
    }

    try {
        // $sql =  "UPDATE token SET token.data_delecao = now() WHERE token.hash = :hash ";
        // $db = getConnection();
        // $stmt = $db->prepare($sql);
        // $stmt->bindParam(":hash", $token['hash']);
        // $stmt->execute();

/*
DELETE FROM resposta_buscada WHERE resposta_buscada.id_busca IN (SELECT id FROM busca WHERE busca.id_usuario = 29);
DELETE FROM busca WHERE busca.id_usuario = 29;
DELETE FROM resposta_buscada WHERE resposta_buscada.id_mensagem_anuncio IN (SELECT mensagem_anuncio.id FROM mensagem_anuncio INNER JOIN mensagem ON mensagem.id = mensagem_anuncio.id_mensagem WHERE mensagem.id_usuario = 29);
DELETE FROM mensagem_anuncio WHERE mensagem_anuncio.id_mensagem IN (SELECT mensagem.id FROM mensagem WHERE mensagem.id_usuario = 29);
DELETE FROM mensagem WHERE mensagem.id IN (SELECT mensagem_direta.id_mensagem FROM mensagem_direta WHERE mensagem_direta.id_usuario = 29);
DELETE FROM mensagem_direta WHERE mensagem_direta.id_usuario = 29;
DELETE FROM mensagem_direta WHERE mensagem_direta.id_mensagem = (SELECT mensagem.id FROM mensagem WHERE mensagem.id_usuario = 29)

REMOVER COMPLETAMENTE
 */
        $auditoria =[];

        {
            $sql =  "SELECT * FROM resposta_buscada WHERE resposta_buscada.id_busca IN (SELECT id FROM busca WHERE busca.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['resposta_buscada'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $sql =  "SELECT * FROM busca WHERE busca.id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['busca'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $sql =  "SELECT * FROM resposta_buscada WHERE resposta_buscada.id_mensagem_anuncio IN (SELECT mensagem_anuncio.id FROM mensagem_anuncio INNER JOIN mensagem ON mensagem.id = mensagem_anuncio.id_mensagem WHERE mensagem.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['criterios_de_anuncio'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $sql =  "SELECT * FROM mensagem_anuncio WHERE mensagem_anuncio.id_mensagem IN (SELECT mensagem.id FROM mensagem WHERE mensagem.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['anuncio'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $sql =  "SELECT * FROM mensagem WHERE mensagem.id IN (SELECT mensagem_direta.id_mensagem FROM mensagem_direta WHERE mensagem_direta.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['mensagens_recebidas'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $sql =  "SELECT * FROM mensagem WHERE mensagem.id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['mensagens_enviadas'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        // {
        //     $sql =  "SELECT * FROM mensagem_direta WHERE mensagem_direta.id_usuario = :id_usuario);";
        //     $db = getConnection();
        //     $stmt = $db->prepare($sql);
        //     $stmt->bindParam(":id_usuario", $token['id_usuario']);
        //     $stmt->execute();
        //     $auditoria['recebidas'] = json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
        // }
        {
            $sql =  "SELECT * FROM resposta_marcada WHERE id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['respostas'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $sql =  "SELECT * FROM usuario WHERE id = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['usuario'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $log = json_encode($auditoria);
            error_log(var_export($log, true));
            // usuario
            $sql = "INSERT INTO auditoria (acao, log) VALUES ('USER_COMPLETE_REMOVE', :log);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":log", $log);
            $stmt->execute();
        }
{

}
        {
            $sql =  "DELETE FROM resposta_buscada WHERE resposta_buscada.id_busca IN (SELECT id FROM busca WHERE busca.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM busca WHERE busca.id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM resposta_buscada WHERE resposta_buscada.id_mensagem_anuncio IN (SELECT mensagem_anuncio.id FROM mensagem_anuncio INNER JOIN mensagem ON mensagem.id = mensagem_anuncio.id_mensagem WHERE mensagem.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM mensagem_anuncio WHERE mensagem_anuncio.id_mensagem IN (SELECT mensagem.id FROM mensagem WHERE mensagem.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM mensagem WHERE mensagem.id IN (SELECT mensagem_direta.id_mensagem FROM mensagem_direta WHERE mensagem_direta.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM mensagem WHERE mensagem.id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM mensagem_direta WHERE mensagem_direta.id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM resposta_marcada WHERE id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM usuario WHERE id = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }

        formUserRemoverOk();

    } catch (Exception $e) {
        //
    }

    return $response->withStatus(200);
});
/*
CREATE TABLE `auditoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `acao` longtext,
  `log` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;
 */
$app->get('/token/usuario/remover/{hash}', function (Request $request, Response $response, array $args) {
    $data['debug'][] = ['$args' => $args];
    try {
      $sql =  "SELECT token.id_usuario, token.acao " .
              "FROM token " .
              "WHERE token.acao = 'USER_COMPLETE_REMOVE' AND token.hash = :hash " .
              "AND now() < token.data_delecao ";
      $db = getConnection();
      $stmt = $db->prepare($sql);
      $stmt->bindParam(":hash", $args['hash']);
      $stmt->execute();
      $tokens = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($tokens) == 0) return $response->withStatus(403);
      $token = $tokens[0];
      $data['debug'][] = ['$tokens' => $tokens, '$token' => $token];
      formUserRemover($args);
    } catch (Exception $e) {
    }
    return $response->withStatus(200);
});
