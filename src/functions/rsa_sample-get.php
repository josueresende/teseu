<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/rsa/sample', function (Request $request, Response $response, array $args) {

  error_reporting(E_ALL);

  define('CRYPT_RSA_PKCS15_COMPAT', true);

  $sessions = array();
  if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));

  $uid = $request->hasHeader('UID') ? $request->getHeader('UID')[0] : $request->getQueryParam("UID");
  $text = $request->getQueryParam("text");

  if ($text == null) $text = "Lorem ipsum dolor ... bla bla bla";

	$data = array();

	$keyPair = (new RSA())->createKey(768);
  $session = array();
  if (!empty($uid) and @array_key_exists($uid, $sessions)) {
    $session = $sessions[$uid];
    $keyPair['privatekey'] = $session['local.private'];
    $keyPair['publickey'] = $session['local.public'];
  }

  $_bool = [];

  $signature = "";
  $verify = false;

  $encrypted = "";
  $decrypted = "";

  $rsaPrivateKey = new RSA();

  $rsaPublicKey = new RSA();

  {
    $rsaPrivateKey->setPublicKeyFormat(RSA::PUBLIC_FORMAT_PKCS8);
    $rsaPrivateKey->setHash('sha256');
    $rsaPrivateKey->loadKey($keyPair['privatekey']);
    $rsaPrivateKey->setSignatureMode(RSA::SIGNATURE_PKCS1);
    $rsaPrivateKey->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
    $signature = $rsaPrivateKey->sign($text);
  }
  {
    $rsaPublicKey->setPublicKeyFormat(RSA::PUBLIC_FORMAT_PKCS8);
    $rsaPublicKey->setHash('sha256');
  	$rsaPublicKey->loadKey($keyPair['publickey']);
    $rsaPublicKey->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
    $rsaPublicKey->setSignatureMode(RSA::SIGNATURE_PKCS1);
    $encrypted = $rsaPublicKey->encrypt($text);
  }
  {
    $decrypted = $rsaPrivateKey->decrypt($encrypted);
  }
  {
    $verify = $rsaPublicKey->verify($text, $signature);
  }

	$data = array(
	  'UID' => $uid,
    'VALIDATION' => $request->getAttribute('VALIDATION'),
	  'session' => $session,
		'keyPair' => $keyPair,
    'text' => $text,
		'signature (made by private key)' => base64_encode($signature),
    'verify (with public key)' => $verify,
		'encrypted (made by public key)' => base64_encode($encrypted),
    'decrypted (with private key)' => $decrypted,
    // '$rsaPublicKey ' => $rsaPublicKey,
    // '$rsaPrivateKey ' => $rsaPrivateKey,
    '$_bool' => $_bool,
	);
  {
//     $rsa = new RSA();
//     $rsa->loadKey('-----BEGIN PUBLIC KEY-----
// MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVx
// wTCpvKe4eCZ0FPqri0cb2JZfXJ/DgYSF6vUpwmJG8wVQZKjeGcjDOL5UlsuusFnc
// CzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ51s1SPrCBkedbNf0T
// p0GbMJDyR4e9T04ZZwIDAQAB
// -----END PUBLIC KEY-----');
//     $sig = pack('H*', '1bd29a1d704a906cd7f726370ce1c63d8fb7b9a620871a05f3141a311c0d6e75fefb5d36dfb50d3ea2d37cd67992471419bfadd35da6e13b494' . '058ddc9b568d4cfea13ddc3c62b86a6256f5f296980d1131d3eaec6089069a3de79983f73eae20198a18721338b4a66e9cfe80e4f8e4fcef7a5bead5cbb' . 'b8ac4c76adffbc178c');
//     // $data['test_sig'] = $sig;
//     $data['test_verify'] = $rsa->verify('zzzz', $sig);
  }

	return $response->write('<pre>'.var_export($data, true).'</pre>');
	// return $response->withJson($data);
});
