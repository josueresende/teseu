<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

function formUserRegisterOk() { ?>
<!DOCTYPE html>
<html>
 <head>
   <meta charset="UTF-8">
   <title></title>
   <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
   <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link href="/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
 </head>
 <body class="login-page">
   <div class="login-box">
     <div class="login-logo">
       <!-- Login  -->
     </div><!-- /.login-logo -->
     <div class="login-box-body">
     	<img src="/images/revolution_logo.png" width="320px;"><br/><br/>
       <div class="row">
           <div class="col-md-12">
           </div>
       </div>
       <div class="row">
           <div class="col-md-12">
             Seu perfil foi habilitado.
           </div>
       </div>
     </div><!-- /.login-box-body -->
   </div><!-- /.login-box -->
   <script src="/js/jQuery-2.1.4.min.js"></script>
   <script src="/js/bootstrap.min.js" type="text/javascript"></script>
 </body>
</html>
<?php }

function formUserRegister($config) { ?>
<!DOCTYPE html>
<html>
 <head>
   <meta charset="UTF-8">
   <title></title>
   <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
   <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link href="/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
 </head>
 <body class="login-page">
   <div class="login-box" style="width: 70% !important;">
     <div class="login-logo">
       <!-- Login  -->
     </div><!-- /.login-logo -->
     <div class="login-box-body">
     	<img src="/images/revolution_logo.png" width="320px;"><br/><br/>
       <div class="row">
           <div class="col-md-12">
           </div>
       </div>
       <div class="form-group has-feedback">
         <div id="texte_a_afficher" class="texte_inselectionnable" style="height: 70%;">
         <span class="stabilisation"></span><p style="text-align: center;"><strong>TERMOS E CONDIÇÕES GERAIS DE USO </strong><strong></strong></p>
         <p style="text-align: center;"><strong></strong><strong></strong><strong>DO APLICATIVO "<span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span>"</strong></p>
         <!--
           -->
         <p style="text-align: center;"><em>.</em></p>
         <p style="text-align: center;"><em>.</em></p>
         <p style="text-align: center;"><em>.</em></p>
         <p style="text-align: left; padding-left: 30px;"><i>texto e logo em desenvolvimento</i></p>
         <!--
         <p style="text-align: left;"><strong><br><br></strong>Estes termos e condições gerais de uso aplicam-se aos serviços prestados pela pessoa jurídica <strong><span id="span_id_nome_empresarial">Ariadne Mobile App</span></strong>, devidamente registrada sob o CNPJ n. <span id="span_id_cnpj_site"><span class="variable_vide">________</span></span>, e-mail: <span id="span_id_email_sitepj"><span class="variable_vide">________</span></span>, com sede em:</p>
         <p style="text-align: left; padding-left: 30px;"><span id="span_id_endereco_sede_site"><span class="variable_vide">________</span></span></p>
         <p style="text-align: left; padding-left: 30px;">representada por:</p>
         <p style="text-align: left; padding-left: 30px;"><strong><span id="span_id_socio1_sitepj"><span class="variable_vide">________</span></span></strong>, CPF n. <span id="span_id_cpf_socio1_sitepj"><span class="variable_vide">________</span></span>, <span id="span_id_documento_socio1_sitepj">Carteira de Identidade (RG)</span> n. <span id="span_id_numero_documento_socio1_sitepj"><span class="variable_vide">________</span></span>, expedida  por <span id="span_id_orgao_expedidor_socio1_sitepj"><span class="variable_vide">________</span></span></p>

         <p><br>doravante denominada <strong>EDITOR DO</strong> <strong style="text-align: center;"></strong><strong style="text-align: center;">APLICATIVO</strong>,</p>
         <p>por meio do aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong>.</p>
         <p><strong><br><br>I. DO OBJETO</strong></p>
         <p>O aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong> caracteriza-se pela prestação dos seguintes serviços:</p>
         <p style="padding-left: 30px;"><span id="span_id_objeto_site">Informações para questionário de pesquisa, localização para determinar proximidade na pesquisa</span></p>
         <p><strong><br><br>II. DA ACEITAÇÃO DOS TERMOS E CONDIÇÕES GERAIS DE USO </strong><strong style="text-align: center;"></strong></p>
         <p>Todos aqueles que desejarem ter acesso aos serviços ofertados através do aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong> deverão, primeiramente, se informar sobre as regras que compõem o presente instrumento, as quais ficarão disponíveis para pronta e ampla consulta, em link direto no próprio aplicativo.</p>
         <p>Ao utilizar o aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong>, o usuário aceita integralmente as presentes normas e compromete-se a observá-las, sob risco de aplicação das penalidades cabíveis. Antes de iniciar qualquer navegação no aplicativo, o usuário deverá cientificar-se de eventuais modificações ou atualizações ocorridas neste termos.</p>
         <p>O usuário poderá ainda ser avisado por e-mail ou diretamente no próprio aplicativo sobre eventuais mudanças nas presentes normas.</p>
         <p>Caso não concorde com quaisquer das regras aqui descritas, o usuário deve, imediatamente, abster-se de utilizar o serviço. Se for de seu interesse, poderá, ainda, entrar em contato com o serviço de atendimento ao cliente, para apresentar-lhe as suas ressalvas.</p>
         <p><strong><br><br>III. DA NAVEGAÇÃO</strong></p>
         <p>O editor do aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong> se compromete a utilizar todas as soluções técnicas à sua disposição para permitir o acesso ao serviço 24 (vinte e quatro) horas por dia, 7 (sete) dias por semana. Entretanto, ele poderá, a qualquer momento, interromper, limitar ou suspender o acesso ao aplicativo ou a algumas de suas páginas, a fim de realizar atualizações, modificações de conteúdo ou qualquer outra ação julgada necessária para o seu bom funcionamento.</p>
         <p>Os presentes termos e condições gerais de uso  se aplicam a todas as extensões do aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong> em redes sociais ou em comunidades, tanto as já existentes, quanto aquelas ainda a serem implementadas.</p>
         <p><strong><br><br>IV. DA GESTÃO </strong><strong>DO APLICATIVO</strong></p>
         <p>Para a boa gestão, o editor do aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong> poderá, a qualquer momento:</p>
         <p style="padding-left: 30px;">a) suspender, interromper ou limitar o acesso a todo ou a parte do aplicativo a uma categoria específica de internautas;</p>
         <p style="padding-left: 30px;">b) remover toda informação que possa perturbar o funcionamento do aplicativo ou que esteja em conflito com normas de Direito brasileiro ou de Direito internacional;</p>
         <p style="padding-left: 30px;">c) suspender o aplicativo, a fim de realizar atualizações e modificações.</p>
         <p><strong><br><br>V. DO CADASTRO</strong></p>
         <p>Os serviços disponibilizados no aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong> apenas poderão ser acessados por pessoas plenamente capazes, conforme o Direito brasileiro. Todos aqueles que não possuírem plena capacidade civil - menores de 18 anos não emancipados, pródigos, ébrios habituais ou viciados em tóxicos e pessoas que não puderem exprimir sua vontade, por motivo transitório ou permanente - deverão ser devidamente assistidos por seus representantes legais, que se responsabilizarão pelo cumprimento das presentes regras.</p>
         <p>Ao usuário, será permitido manter apenas uma conta junto ao aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong>. Contas duplicadas serão automaticamente desativadas pelo editor do aplicativo, sem prejuízo de demais penalidades cabíveis.</p>
         <p>Para o devido cadastramento junto ao serviço, o usuário deverá fornecer integralmente os dados requeridos. Todas as informações fornecidas pelo usuário devem ser precisas, verdadeiras e atualizadas. Em qualquer caso, o usuário responderá, em âmbito cível e criminal, pela veracidade, exatidão e autenticidade dos dados informados.</p>
         <p>O usuário deverá fornecer um endereço de e-mail válido, através do qual o aplicativo realizará todos os contatos necessários. Todas as comunicações enviadas para o referido endereço serão consideradas lidas pelo usuário, que se compromete, assim, a consultar regularmente suas mensagens recebidas e a respondê-las em prazo razoável.</p>
         <p>Após a confirmação de seu cadastro, o usuário possuirá um login (apelido) e uma senha pessoais, que deverão ser por ele utilizados para o acesso a sua conta no aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong>. Estes dados de conexão não poderão ser informados pelo usuário a terceiros, sendo de sua inteira responsabilidade o uso que deles seja feito. O usuário compromete-se a comunicar imediatamente ao editor do aplicativo quaisquer atividades suspeitas ou inesperadas em sua conta.</p>
         <p>Não será permitido ceder, vender, alugar ou transferir, de qualquer forma, a conta, que é pessoal e intransferível.</p>
         <p>Será automaticamente descadastrado o usuário que descumprir quaisquer das normas contidas no presente instrumento, sendo-lhe vedado realizar nova inscrição no aplicativo.</p>
         <p>O usuário poderá, a qualquer tempo e sem necessidade de justificação, requerer o cancelamento de seu cadastro junto ao aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong>. O seu descadastramento será realizado o mais rapidamente possível, desde que não sejam verificados débitos em aberto.</p>
         <p><strong><br><br>VI. DAS RESPONSABILIDADES</strong></p>
         <p>O editor se responsabilizará pelos defeitos ou vícios encontrados nos serviços prestados pelo aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong>, desde que tenha lhes dado causa. Defeitos ou vícios técnicos ou operacionais originados no próprio sistema do usuário não serão de responsabilidade do editor.</p>
         <p>O editor responsabiliza-se apenas pelas informações que foram por ele diretamente divulgadas. Quaisquer informações incluídas pelos usuários, tais como em comentários e em perfis pessoais, serão de inteira responsabilidade dos próprios.</p>
         <p>O usuário é responsável, ainda:</p>
         <p style="padding-left: 30px;">a) pela correta utilização do aplicativo e de seus serviços, prezando pela boa convivência, pelo respeito e pela cordialidade no relacionamento com os demais usuários;</p>
         <p style="padding-left: 30px;">b) pelo cumprimento das regras contidas neste instrumento, bem como normas de Direito nacional e de Direito internacional;</p>
         <p style="padding-left: 30px;">c) pela proteção dos dados de acesso à sua conta (login e senha).</p>
         <p>O editor não será responsável:</p>
         <p style="padding-left: 30px;">a) pelas características intrínsecas da internet, principalmente relativas à confiabilidade e à procedência das informações circulando nesta rede;</p>
         <p style="padding-left: 30px;">b) pelos conteúdos ou atividades ilícitas praticadas através de seu aplicativo.</p>
         <p><strong><br><br>VII. DOS LINKS EXTERNOS</strong></p>
         <p>O aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong> pode conter links externos redirecionando o usuário para outras páginas da internet, sobre os quais o editor não exerce controle. Apesar das verificações prévias e regulares realizadas pelo editor, ele se isenta de qualquer responsabilidade sobre o conteúdo encontrado nestes sites e serviços.</p>
         <p>Poderão ser incluídos links nas páginas e nos documentos do aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong>, desde que não sirvam para fins comerciais ou publicitários. Esta inclusão dependerá de autorização prévia do editor.</p>
         <p>Não será autorizada a inclusão de páginas que divulguem quaisquer tipos de informações ilícitas, violentas, polêmicas, pornográficas, xenofóbicas, discriminatórias ou ofensivas.</p>
         <p><strong><br><br>VIII. DOS DIREITOS AUTORAIS</strong></p>
         <p>A estrutura do aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong>, bem como os textos, os gráficos, as imagens, as fotografias, os sons, os vídeos e as demais aplicações informáticas que o compõem são de propriedade do editor e são protegidas pela legislação brasileira e internacional referente à propriedade intelectual.</p>
         <p>Qualquer representação, reprodução, adaptação ou exploração parcial ou total dos conteúdos, marcas e serviços propostos pelo aplicativo, por qualquer meio que seja, sem autorização prévia, expressa e escrita do editor, é estritamente vedada, podendo-se recorrer às medidas cíveis e penais cabíveis. Estão excluídos desta previsão apenas os elementos que estejam expressamente designados no aplicativo como livres de direitos autorais.</p>
         <p>O acesso não gera para o usuário qualquer direito de propriedade intelectual relativo a elementos do aplicativo, os quais restam sob a propriedade exclusiva do editor.</p>
         <p>É vedado ao usuário incluir no aplicativo dados que possam modificar o seu conteúdo ou sua aparência.</p>
         <p><strong><br><br>IX. DA POLÍTICA DE PRIVACIDADE</strong></p>
         <p><strong>1. Informações gerais</strong></p>
         <p><strong>Essa seção contém informações a respeito do tratamento de dados pessoais do usuário, total ou parcialmente, automatizados ou não, realizado pelo</strong> <strong>aplicativo</strong> <strong>e que poderão ou não serem armazenados. O objetivo é fornecer orientações a respeito das informações coletadas, os motivos da coleta e como o usuário poderá atualizar, gerenciar, exportar ou excluir essas informações.</strong></p>
         <p><strong>A política de privacidade do</strong> <strong>aplicativo</strong> <strong>está de acordo com a Lei federal n. 12.965 de 23 de abril de 2014 (Marco Civil da Internet), com a </strong><strong>Lei federal n. 13.709, de 14 de agosto de 2018 (Lei de Proteção de Dados Pessoais)</strong><strong> e com o Regulamento UE n. 2016/679 de 27 de abril de 2016 (Regulamento Geral Europeu de Proteção de Dados Pessoais).</strong></p>
         <p><strong>Esta política de privacidade poderá ser atualizada em decorrência de uma eventual atualização normativa, razão pela qual se convida o usuário a consultar periodicamente esta seção.</strong></p>
         <p><strong><br>2. Direitos do usuário</strong></p>
         <p><strong>O </strong><strong>aplicativo</strong> <strong>se compromete a cumprir as normas dispostas pelo Regulamento Geral Europeu de Proteção de Dados Pessoais (RGPD), em respeito aos seguintes princípios:</strong></p>
         <ul>
         <li><strong>Seus dados pessoais serão processados de forma lícita, leal e transparente (licitude, lealdade e transparência);</strong></li>
         <li><strong>Seus dados pessoais serão coletados apenas para finalidades determinadas, explícitas e legítimas, não podendo ser tratados posteriormente de uma forma incompatível com essas finalidades (limitação das finalidades);</strong></li>
         <li><strong>Seus dados pessoais serão coletados de forma adequada, pertinente e limitada às necessidades do objetivo para os quais eles são processados (minimização dos dados);</strong></li>
         <li><strong>Seus dados pessoais serão exatos e atualizados sempre que necessário, de maneira que os dados inexatos sejam apagados ou retificados quando possível (exatidão);</strong></li>
         <li><strong>Seus dados pessoais serão conservados de uma forma que permita a identificação dos titulares dos dados apenas durante o período necessário para as finalidades para as quais são tratados (limitação da conservação);</strong></li>
         <li><strong>Seus dados pessoais serão tratados de forma segura, protegidos do tratamento não autorizado ou ilícito e contra a sua perda, destruição ou danificação acidental, adotando as medidas técnicas ou organizativas adequadas (integridade e confidencialidade).</strong></li>
         </ul>
         <p><strong>O usuário do</strong> <strong>aplicativo</strong> <strong>possui os seguintes direitos, conferidos pela </strong><strong>Lei federal n. 13.709, de 14 de agosto de 2018 (Lei de Proteção de Dados Pessoais) e </strong><strong>pelo</strong> <strong>Regulamento Geral Europeu de Proteção de Dados Pessoais (RGPD):</strong></p>
         <ul>
         <li><strong>Direito de confirmação e acesso: é o direito do usuário de obter do </strong><strong>aplicativo</strong> <strong>a confirmação de que os dados pessoais que lhe digam respeito são ou não objeto de tratamento e, se for esse o caso, o direito de acessar os seus dados pessoais;</strong></li>
         <li><strong>Direito de retificação: é o direito do usuário de obter <strong>do </strong></strong><strong>aplicativo</strong><strong>, sem demora injustificada,</strong> <strong>a retificação dos dados pessoais inexatos que lhe digam respeito;</strong></li>
         <li><strong>Direito à eliminação dos dados (direito ao esquecimento): é o direito do usuário de ter seus dados apagados do</strong> <strong>aplicativo</strong><strong>;</strong></li>
         <li><strong>Direito à limitação do tratamento dos dados: é o direito do usuário de limitar o tratamento de seus dados pessoais, podendo obtê-la quando contesta a exatidão dos dados, quando o tratamento for ilícito, quando o</strong> <strong>aplicativo</strong> <strong>não precisar mais dos dados para as finalidades propostas e quando tiver se oposto ao tratamento dos dados </strong><strong>e em caso de tratamento de dados desnecessários;</strong></li>
         <li><strong>Direito de oposição: é o direito do usuário de, a qualquer momento, se opor por motivos relacionados com a sua situação particular, ao tratamento dos dados pessoais que lhe digam respeito, podendo se opor ainda ao uso de seus dados pessoais para definição de perfil de marketing (profiling);</strong></li>
         <li><strong>Direito de portabilidade dos dados: é o direito do usuário de receber os dados pessoais que lhe digam respeito e que tenha fornecido ao</strong> <strong>aplicativo</strong><strong>, num formato estruturado, de uso corrente e de leitura automática, e o direito de transmitir esses dados a outro</strong> <strong>aplicativo</strong><strong>;</strong></li>
         <li><strong>Direito de não ser submetido a decisões automatizadas: é o direito do usuário de não ficar sujeito a nenhuma decisão tomada exclusivamente com base no tratamento automatizado, incluindo a definição de perfis (profiling), que produza efeitos na sua esfera jurídica ou que o afete significativamente de forma similar.</strong></li>
         </ul>
         <p><strong>O usuário poderá exercer os seus direitos por meio de comunicação escrita enviada ao</strong> <strong>aplicativo</strong> <strong>com o assunto "RGDP-<span id="span_id_url_site"><span class="variable_vide">________</span></span>", especificando:</strong></p>
         <ul>
         <li><strong>Nome completo ou razão social, número do CPF (Cadastro de Pessoas Físicas, da Receita Federal do Brasil) ou CNPJ (Cadastro Nacional de Pessoa Jurídica, da Receita Federal do Brasil) e endereço de e-mail do usuário e, se for o caso, do seu representante;</strong></li>
         <li><strong>Direito que deseja exercer junto ao</strong> <strong>aplicativo</strong><strong>;</strong></li>
         <li><strong>Data do pedido e assinatura do usuário;</strong></li>
         <li><strong>Todo documento que possa demonstrar ou justificar o exercício de seu direito.</strong></li>
         </ul>
         <p><strong>O pedido deverá ser enviado ao e-mail: <span id="span_id_email_rgdp"><span class="variable_vide">________</span></span>, ou por correio, ao seguinte endereço:</strong></p>
         <p style="padding-left: 30px;"><strong><span id="span_id_endereco_rgdp"><span class="variable_vide">________</span></span></strong></p>
         <p><strong>O usuário será informado em caso de retificação ou eliminação dos seus dados.</strong></p>
         <p><strong><br>3. Informações coletadas</strong></p>
         <p><strong>3.1. Tipo de dados coletados</strong></p>
         <p><strong>Os dados pessoais coletados pelo</strong> <strong>aplicativo</strong> <strong>serão aqueles necessários à identificação do usuário, bem como os dados sensíveis do usuário, nos termos dos artigos 9º e 10 do Regulamento Geral Europeu de Proteção de Dados Pessoais (RGPD) e </strong><strong>do artigo 11 da Lei federal n. 13.709, de 14 de agosto de 2018.</strong></p>
         <p><strong>Entende-se por dados sensíveis os dados pessoais que revelem a origem racial ou étnica, as opiniões políticas, as convicções religiosas ou filosóficas, ou a filiação sindical, bem como o tratamento de dados genéticos, dados biométricos para identificar uma pessoa de forma inequívoca, dados relativos à saúde ou dados relativos à vida sexual ou orientação sexual de uma pessoa. Também são classificados como dados sensíveis os dados pessoais relacionados com condenações penais e infrações ou com medidas de segurança conexas.</strong></p>
         <p><strong>Serão coletados todos os dados necessários ao cadastro do usuário, como nome completo ou razão social, número do CPF ou CNPJ, e-mail e endereço do usuário, requisitos necessários para acessar determinados serviços oferecidos pelo </strong><strong>aplicativo</strong><strong>.</strong></p>

         <p><strong>Eventualmente poderão ser coletados outras categorias de dados, desde que fornecidos com o consentimento do usuário, ou ainda em razão de interesse legítimo ou demais motivos permitidos em lei.</strong></p>
         <p><strong>O usuário se compromete a fornecer exclusivamente os seus dados pessoais e não os de terceiros.</strong></p>
         <p><strong>3.2. Fundamento jurídico para o tratamento dos dados pessoais</strong></p>
         <p><strong>Ao utilizar os serviços</strong> <strong>aplicativo</strong><strong>,</strong> <strong>o usuário está consentindo com o presente termo e com a sua política de privacidade.</strong></p>
         <p><strong>O usuário tem o direito de retirar o seu consentimento a qualquer momento, não comprometendo a licitude dos dados pessoais tratados antes de sua retirada. A retirada do consentimento poderá ser feita pelo e-mail: <span id="span_id_email_rgdp"><span class="variable_vide">________</span></span>, ou por correio enviado ao seguinte endereço:</strong></p>
         <p style="padding-left: 30px;"><strong><span id="span_id_endereco_rgdp"><span class="variable_vide">________</span></span></strong></p>
         <p><strong>O consentimento dos relativamente ou absolutamente incapazes, especialmente de crianças menores de 16 (dezesseis) anos, apenas poderá ser feito, respectivamente, se devidamente assistidos ou representados.</strong></p>

         <p><strong>O tratamento de dados pessoais sem o consentimento do usuário apenas será realizado em razão de interesse legítimo ou para as hipóteses previstas em lei, dentre outras, as seguintes:</strong></p>
         <ul>
         <li><strong>para o cumprimento de obrigação legal ou regulatória pelo controlador;</strong></li>
         <li><strong>para a realização de estudos por órgão de pesquisa, garantida, sempre que possível, a anonimização dos dados pessoais;</strong></li>
         <li><strong>quando necessário para a execução de contrato ou de procedimentos preliminares relacionados a contrato do qual seja parte o usuário, a pedido do titular dos dados;</strong></li>
         <li><strong>para o exercício regular de direitos em processo judicial, administrativo ou arbitral, esse último nos termos da Lei nº 9.307, de 23 de setembro de 1996 (Lei de Arbitragem);</strong></li>
         <li><strong>para a proteção da vida ou da incolumidade física do titular dos dados ou de terceiro;</strong></li>
         <li><strong>para a tutela da saúde, em procedimento realizado por profissionais da área da saúde ou por entidades sanitárias;</strong></li>
         <li><strong>quando necessário para atender aos interesses legítimos do controlador ou de terceiro, exceto no caso de prevalecerem direitos e liberdades fundamentais do titular dos dados que exijam a proteção dos dados pessoais;</strong></li>
         <li><strong>para a proteção do crédito, inclusive quanto ao disposto na legislação pertinente.</strong></li>
         </ul>
         <p><strong>3.3. Finalidades do tratamento dos dados pessoais</strong></p>
         <p><strong>Os dados pessoas do usuário coletados pelo</strong> <strong>aplicativo</strong> <strong>têm por finalidade facilitar, agilizar e cumprir os compromissos estabelecidos com o usuário e a fazer cumprir as solicitações realizadas por meio do preenchimento de formulários.</strong></p>
         <p><strong>Os dados pessoais poderão ser utilizados também com uma finalidade comercial, para personalizar o conteúdo oferecido ao usuário, bem como para dar subsídio ao</strong> <strong>aplicativo</strong> <strong>para a melhora da qualidade e funcionamento de seus serviços.</strong></p>
         <p><strong>O</strong> <strong>aplicativo</strong> <strong>recolhe os dados do usuário para que seja realizado definição de perfis (profiling), ou seja, tratamento automatizado de dados pessoais que consista em utilizar esses dados para avaliar certos aspetos pessoais do usuário, principalmente para analisar ou prever aspectos relacionados com o seu desempenho profissional, a sua situação econômica, saúde, preferências pessoais, interesses, fiabilidade, comportamento, localização ou deslocamentos.</strong></p>
         <p><strong>Os dados de cadastro serão utilizados para permitir o acesso ao usuário de determinados conteúdos do </strong><strong>aplicativo</strong><strong> exclusivos para usuários cadastrados.</strong></p>

         <p><strong>Se o</strong> <strong>aplicativo</strong> <strong>tiver a intenção de tratar os dados pessoais do usuário para outras finalidades ele deverá ser informado sobre as outras finalidades, que deverão ser feitas em observação aos mesmos direitos e obrigações.</strong></p>
         <p><strong>3.4. Prazo de conservação dos dados pessoais</strong></p>
         <p><strong>Os dados pessoais do usuário serão conservados por um período não superior ao exigido para cumprir os objetivos em razão dos quais eles são processados.</strong></p>
         <p><strong>O período de conservação dos dados são definidos de acordo com os seguintes critérios:</strong></p>
         <p style="padding-left: 30px;"><strong><span id="span_id_criterio_conservacao">O usuário da plataforma poderá solicitar sua remoção através do aplicativo.</span></strong></p>
         <p><strong>Os dados pessoais dos usuários apenas poderão ser conservados após o término de seu tratamento nas seguintes hipóteses:</strong></p>
         <ul>
         <li><strong>para o cumprimento de obrigação legal ou regulatória pelo controlador;</strong></li>
         <li><strong>para estudo por órgão de pesquisa, garantida, sempre que possível, a anonimização dos dados pessoais;</strong></li>
         <li><strong>para a transferência a terceiro, desde que respeitados os requisitos de tratamento de dados dispostos na legislação;</strong></li>
         <li><strong>para uso exclusivo do controlador, vedado seu acesso por terceiro, e desde que anonimizados os dados.</strong></li>
         </ul>
         <p><strong>3.5. Destinatários e transferência dos dados pessoais</strong></p>
         <p><strong>Os dados pessoas do usuário poderão ser compartilhados com as seguintes pessoas ou empresas:</strong></p>
         <p style="padding-left: 30px;"><strong><span id="span_id_pessoas_transferencia_dados"><span class="variable_vide">________</span></span></strong></p>
         <p><strong>A transferência apenas poderá ser feita para outro país caso o país ou território em questão ou a organização internacional em causa asseguram um nível de proteção adequado dos dados do usuário.</strong></p>
         <p><strong>Caso não haja nível de proteção adequado, o</strong> <strong>aplicativo</strong> <strong>se compromete a garantir a proteção dos seus dados de acordo com as regras mais rigorosas, por meio de cláusulas contratuais específicas para determinada transferência, cláusulas-padrão contratuais, normas corporativas globais ou selos, certificados e códigos de conduta regularmente emitidos.</strong></p>
         <p><strong><br>4. Do tratamento dos dados pessoais</strong></p>
         <p><strong>4.1. Do responsável pelo tratamento dos dados (data controller)</strong></p>
         <p><strong>O controlador, responsável pelo tratamento dos dados pessoais do usuário, é a pessoa física ou jurídica, a autoridade pública, a agência ou outro organismo que, individualmente ou em conjunto com outras, determina as finalidades e os meios de tratamento de dados pessoais.</strong></p>

         <p><strong>Neste </strong><strong>aplicativo</strong><strong>, o responsável pelo tratamento dos dados pessoais coletados é</strong> <strong><span id="span_id_nome_empresarial">Ariadne Mobile App</span>, representada por</strong> <strong><span id="span_id_socio1_sitepj"><span class="variable_vide">________</span></span>, que poderá ser contactado</strong> <strong>pelo e-mail: <span id="span_id_email_sitepj"><span class="variable_vide">________</span></span> ou no endereço:</strong></p>
         <p style="padding-left: 30px;"><strong><span id="span_id_endereco_sede_site"><span class="variable_vide">________</span></span></strong></p>
         <p><strong>O</strong> <strong>aplicativo</strong> <strong>possui também os seguintes responsáveis pelo tratamento dos dados pessoais coletados:</strong></p>
         <p style="padding-left: 30px;"><strong><span id="span_id_descricao_mais_controllers"><span class="variable_vide">________</span></span></strong></p>

         <p><strong>O responsável pelo tratamento dos dados se encarregará diretamente do tratamento dos dados pessoais do usuário.</strong></p>
         <p><strong>4.2. Do encarregado de proteção de dados (data protection officer)</strong></p>
         <p><strong>O encarregado de proteção de dados (data protection officer) é o profissional encarregado de informar, aconselhar e controlar o responsável pelo tratamento dos dados</strong> <strong>, bem como os trabalhadores que tratem os dados, a respeito das obrigações do</strong> <strong>aplicativo</strong> <strong>nos termos do Regulamento Geral Europeu de Proteção de Dados (RGDP), da</strong><strong> Lei federal n. 13.709, de 14 de agosto de 2018 (Lei de Proteção de Dados Pessoais) e </strong><strong>de outras disposições de proteção de dados presentes na legislação nacional e internacional, em cooperação com a autoridade de controle competente.</strong></p>
         <p><strong>Neste</strong> <strong>aplicativo</strong> <strong>o encarregado de proteção de dados (data protection officer) é <span id="span_id_nome_dpo"><span class="variable_vide">________</span></span> e poderá ser contactado pelo e-mail: <span id="span_id_email_dpo"><span class="variable_vide">________</span></span>.</strong></p>
         <p><br><strong>5. Segurança no tratamento dos dados pessoais do usuário</strong></p>
         <p><strong>O</strong> <strong>aplicativo</strong><strong> se compromete a aplicar as medidas técnicas e organizativas adequadas para assegurar um nível de segurança adequado ao risco, tendo em conta as técnicas mais avançadas, os custos de aplicação e a natureza, o âmbito, o contexto e as finalidades do tratamento, bem como os riscos, de probabilidade e gravidade variável, para os direitos e liberdades do usuário.</strong></p>

         <p><strong>No entanto,</strong> <strong>o</strong> <strong>aplicativo</strong> <strong>se exime da responsabilidade por culpa exclusiva de terceiro, como em caso de ataque de hackers ou crackers, ou culpa exclusiva do usuário, que, por exemplo, transfere seus dados a terceiro, exceto se a pirataria se deu em razão de falha de segurança do</strong> <strong>aplicativo</strong><strong>. O</strong> <strong>aplicativo</strong> <strong>se compromete, ainda, a comunicar o usuário em prazo adequado caso ocorra algum tipo de violação da segurança de seus dados pessoais que possa lhe causar um alto risco para seus direitos e liberdades pessoais.</strong></p>
         <p><strong>A violação de dados pessoais é uma violação da segurança que provoque, de modo acidental ou ilícito, a destruição, a perda, a alteração, a divulgação ou o acesso, não autorizados, a dados pessoais transmitidos, conservados ou sujeitos a qualquer outro tipo de tratamento.</strong></p>
         <p><strong>Por fim, o</strong> <strong>aplicativo</strong> <strong>se compromete a tratar os dados pessoais do usuário com confidencialidade, dentro dos limites legais.</strong></p>
         <p><br><strong>6</strong>.<strong> Reclamação a uma autoridade de controle</strong></p>
         <p><strong>Sem prejuízo de qualquer outra via de recurso administrativo ou judicial, todos os titulares de dados têm direito a apresentar reclamação a uma autoridade de controle. A reclamação poderá ser feita à autoridade da sede do</strong> <strong>aplicativo</strong><strong>, do país de residência habitual do usuário, do seu local de trabalho ou do local onde foi alegadamente praticada a infração.</strong></p>
         <p><strong><br><br>X. DO SERVIÇO DE ATENDIMENTO AO USUÁRIO</strong></p>
         <p>Em caso de dúvidas, sugestões ou problemas com a utilização do aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong>, o usuário poderá contatar diretamente o seu serviço de atendimento, através do endereço de e-mail: <span id="span_id_email_sac"><span class="variable_vide">________</span></span>, bem como do telefone: <span id="span_id_telefone_sac"><span class="variable_vide">________</span></span>. Estes serviços de atendimento ao usuário estarão disponíveis nos seguintes dias e horários:</p>
         <p style="padding-left: 30px;"><span id="span_id_disponibilidade_sac"><span class="variable_vide">________</span></span></p>
         <p>O usuário poderá, ainda, optar por enviar correspondência ao endereço da sede do aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong>, informado no início deste documento.</p>
         <p><strong><br><br>XI. DAS SANÇÕES</strong></p>
         <p>Sem prejuízo das demais medidas legais cabíveis, o editor do aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong> poderá, a qualquer momento, advertir, suspender ou cancelar a conta do usuário:</p>
         <p style="padding-left: 30px;">a) que descumprir quaisquer dos dispositivos contidos no presente instrumento;</p>
         <p style="padding-left: 30px;">b) que descumprir os seus deveres de usuário;</p>
         <p style="padding-left: 30px;">c) que praticar atos fraudulentos ou dolosos;</p>
         <p style="padding-left: 30px;">d) cujo comportamento constitua ou possa vir a importar ofensa ou dano a terceiro ou ao próprio aplicativo.</p>
         <p><strong><br><br>XII. DAS ALTERAÇÕES</strong></p>
         <p>A presente versão dos termos e condições gerais de uso foi atualizada pela última vez em: <span id="span_id_data_termo" class="encours">01/06/2019</span>.</p>
         <p>O editor se reserva o direito de modificar, a qualquer momento e sem qualquer aviso prévio, o aplicativo e os serviços, bem como as presentes normas, especialmente para adaptá-las às evoluções do aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong>, seja pela disponibilização de novas funcionalidades, seja pela supressão ou modificação daquelas já existentes.</p>
         <p>Dessa forma, convida-se o usuário a consultar periodicamente esta página para verificar as atualizações.</p>
         <p><br><br><br></p>
         -->
         <p style="text-align: center;"><em>.</em></p>
         <p style="text-align: center;"><em>.</em></p>
         <p style="text-align: center;"><em>.</em></p>
         <p style="text-align: center;"><em>Seja bem-vindo(a)!</em></p>
         <p style="text-align: center;"><em>A equipe do aplicativo <strong><span id="span_id_nome_aplicativo">Revolution (Mobile App de voluntários em pesquisas)</span></strong> </em><em>lhe deseja uma excelente navegação!</em></p>
         </div>
       </div>
       <form action="/token/usuario/registrar/<?php echo $config['hash']; ?>" method="post">
         <div class="form-group has-feedback" style="width: 300px;">
           <?php if ($config['password-invalido']) : ?>
            <div class="input row-input with-error has-error text-red">Senha incorreta, tente novamente.</div>
           <?php endif; ?>
           <input type="password" class="form-control" placeholder="Password" name="password" required />
           <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         </div>
         <div class="row" style="width: 300px;">
           <div class="col-xs-8">
             <!-- <div class="checkbox icheck">
               <label>
                 <input type="checkbox"> Remember Me
               </label>
             </div>  -->
           </div><!-- /.col -->
           <div class="col-xs-8">
             <input type="submit" class="btn btn-primary btn-block btn-flat" value="HABILITAR PERFIL" />
           </div><!-- /.col -->
         </div>
       </form>
     </div><!-- /.login-box-body -->
   </div><!-- /.login-box -->
   <script src="/js/jQuery-2.1.4.min.js"></script>
   <script src="/js/bootstrap.min.js" type="text/javascript"></script>
 </body>
</html>
<?php }

$app->post('/token/usuario/registrar/{hash}', function (Request $request, Response $response, array $args) {
    $form = $request->getParsedBody();
    try {
        $sql = "SELECT token.id_usuario, token.acao, usuario.senha, token.hash
                FROM token
                INNER JOIN usuario ON usuario.id = token.id_usuario
                WHERE token.acao = 'USER_REGISTER' AND token.hash = :hash AND now() < token.data_delecao ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":hash", $args['hash']);
        $stmt->execute();
        $tokens = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($tokens) == 0) return $response->withStatus(403);
        $token = $tokens[0];
        $data['debug'][] = ['$tokens' => $tokens, '$token' => $token];
    } catch (Exception $e) {
        //
    }

    if (md5(md5($form['password'])) != $token['senha']) {
        $parametros['password-invalido'] = true;
        formUserRegister($parametros);
        return $response->withStatus(200);
    }

    try {
        $sql =  "UPDATE token SET token.data_delecao = now() WHERE token.hash = :hash ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":hash", $token['hash']);
        $stmt->execute();

        $senha = md5(md5($form['password']));

        $sql =  "UPDATE usuario SET usuario._enabled = 'S' WHERE usuario.id = :id_usuario ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":id_usuario", $token['id_usuario']);
        $stmt->execute();

        formUserRegisterOk();

    } catch (Exception $e) {
        //
    }

    return $response->withStatus(200);
});

$app->get('/token/usuario/registrar/{hash}', function (Request $request, Response $response, array $args) {
    $data['debug'][] = ['$args' => $args];
    try {
      $sql =  "SELECT token.id_usuario, token.acao " .
              "FROM token " .
              "WHERE token.acao = 'USER_REGISTER' AND token.hash = :hash " .
              "AND now() < token.data_delecao ";
      $db = getConnection();
      $stmt = $db->prepare($sql);
      $stmt->bindParam(":hash", $args['hash']);
      $stmt->execute();
      $tokens = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($tokens) == 0) return $response->withStatus(403);
      $token = $tokens[0];
      $data['debug'][] = ['$tokens' => $tokens, '$token' => $token];
      formUserRegister($args);
    } catch (Exception $e) {
    }
    return $response->withStatus(200);
});
