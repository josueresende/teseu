<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->post('/usuario/foto', function (Request $request, Response $response, array $args) {
    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    $form = $request->getParsedBody();
    $id_usuario = 0;
    try {
        $usuario = $form['usuario'];
        if (empty($usuario) == false) {
            $sql = "SELECT * FROM usuario WHERE nome LIKE :usuario ";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":usuario", $usuario);
            $stmt->execute();
            $usuarios = $stmt->fetchAll(PDO::FETCH_OBJ);
            $id_usuario = $usuarios[0]->id;
        }

        if ($id_usuario == 0) {
            $data['error_code'] = 999;
            $data['error_description'] = "Consulta sem criterio.";
        } else {
            $sql = "UPDATE usuario SET foto_base64 = :foto_base64 WHERE id = :id_usuario;";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":foto_base64", $form['foto_base64']);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->execute();
        }
    } catch(PDOException $e) {
	    $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
