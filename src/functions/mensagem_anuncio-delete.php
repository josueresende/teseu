<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->delete('/mensagem/anuncio', function (Request $request, Response $response, array $args) {
    global $sql_mensagens_pesquisador;

    $sessions = array();
    if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );

    $id_anuncio = $request->getQueryParam("idAnuncio");

    $id_usuario = 0;
    $tp_usuario = '';

    try {
        if (@empty($id_anuncio)) $id_busca = 0;
        $data['debug'][] = array('id_anuncio' => $id_anuncio);

        {
            $uid = $request->getHeader('UID')[0];
            $data['debug'][] = array('uid' => $uid);
            $session = array();
            if (@array_key_exists($uid, $sessions)) {
                $session = $sessions[$uid];
                $id_usuario = $session['id_usuario'];
                $tp_usuario = $session['tp_usuario'];
            }
            $data['debug'][] = array('id_usuario' => $id_usuario, 'tp_usuario' => $tp_usuario);
        }

        if ($id_usuario == 0 or $id_anuncio == 0) {
            $data['error_code'] = 999;
            $data['error_description'] = "Delecao sem criterio.";
        } else {

            $sql =
                "UPDATE mensagem_anuncio SET _deleted = 'S'  WHERE id = :id_anuncio AND id_mensagem in (SELECT id FROM mensagem WHERE id_usuario = :id_usuario) " .
                ""
            ;
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_anuncio", $id_anuncio);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->execute();
        }

    } catch(PDOException $e) {
	    $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
