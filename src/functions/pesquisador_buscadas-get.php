<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/pesquisador/buscadas', function (Request $request, Response $response, array $args) {
    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    $id_usuario = 0;
    $id_busca = 0;
    try {
        $usuario = $request->getQueryParam("usuario");
        $id_busca = $request->getQueryParam("idBusca");
        if (empty($usuario) == false) {
            $sql = "SELECT * FROM usuario WHERE nome LIKE :usuario ";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":usuario", $usuario);
            $stmt->execute();
            $usuarios = $stmt->fetchAll(PDO::FETCH_OBJ);
            $id_usuario = $usuarios[0]->id;
        }

        if ($id_usuario == 0 and $id_busca == 0) {
            $data['error_code'] = 999;
            $data['error_description'] = "Consulta sem criterio.";
        } else {

            $sql =
                "SELECT DISTINCT " .
                "busca.id_estudo as estudo_id, " .
                "busca.id as busca_id, " .
                "resposta_buscada.id as buscada_id, " .
                "resposta_buscada.id_questao as questao_id, " .
                "resposta_buscada.id_resposta as resposta_id, " .
                "resposta_buscada.texto as buscada_texto, " .
                "resposta.conteudo as resposta_conteudo, " .
                "resposta.texto as resposta_texto, " .
                "'dummy' as dummy " .
                "FROM busca  " .
                "INNER JOIN resposta_buscada ON resposta_buscada.id_busca = busca.id " .
                "INNER JOIN resposta ON resposta.id = resposta_buscada.id_resposta " .
                "WHERE busca._deleted = 'N' AND ( busca.id_usuario = :id_usuario OR busca.id = :id_busca ) " .
                ""
            ;
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->bindParam(":id_busca", $id_busca);
            $stmt->execute();
            ob_start();
            $stmt->debugDumpParams();
            $debug['sql'] = ob_get_contents();
            ob_end_clean();

            $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);
            $data['respostas'] = $resultado;
        }

        $data['debug'] = array(@var_export($usuarios[0], true),@var_export($stmt, true),@var_export($form, true));

    } catch(PDOException $e) {
	    $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
