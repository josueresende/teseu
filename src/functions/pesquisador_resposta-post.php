<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->post('/pesquisador/resposta', function (Request $request, Response $response, array $args) {

    $id_usuario = $request->getAttribute('ID_USUARIO');
    $tp_usuario = $request->getAttribute('TP_USUARIO');

    error_log(var_export([
        __FILE__ => __LINE__,
        '$id_usuario' => $id_usuario,
        '$tp_usuario' => $tp_usuario,
    ], true));

    if ($request->getAttribute('VALIDATION') == false) return $response->withStatus(403);

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    $form = $request->getParsedBody();
    try {
        // $sql = "SELECT * FROM usuario WHERE nome LIKE :usuario ";
        $db = getConnection();
        // $stmt = $db->prepare($sql);
        // $stmt->bindParam(":usuario", $form['usuario']);
        // $stmt->execute();
        //
        // $usuarios = $stmt->fetchAll(PDO::FETCH_OBJ);
        //
        // $data['debug'] = array(@var_export($usuarios, true),@var_export($stmt, true),@var_export($form, true));

        // if ($usuarios == null) {
        //     $data['error_code'] = 999;
        //     $data['error_description'] = "Usuário não encontrado.";
        // } else {
        //id_questao	id_resposta	id_busca	texto
            $sql = "INSERT INTO resposta_buscada (id_busca, id_questao, id_resposta, texto) VALUES (:id_busca, :id_questao, :id_resposta, :texto);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_busca", $form['idBusca']);
            $stmt->bindParam(":id_questao", $form['idQuestao']);
            $stmt->bindParam(":id_resposta", $form['idResposta']);
            $stmt->bindParam(":texto", $form['expressao']);
            $stmt->execute();
     		//$json->id = $db->lastInsertId();
            //$data['usuario'] = json_encode($form);
        // }
// */
    } catch(PDOException $e) {
	    $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
// */
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
