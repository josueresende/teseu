<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->post('/location/set', function (Request $request, Response $response, array $args) {

    $id_usuario = $request->getAttribute('ID_USUARIO');
    $tp_usuario = $request->getAttribute('TP_USUARIO');

    error_log(var_export([
        __FILE__ => __LINE__,
        '$id_usuario' => $id_usuario,
        '$tp_usuario' => $tp_usuario,
    ], true));

    if ($request->getAttribute('VALIDATION') == false) return $response->withStatus(403);

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );

    // $bodyContents = $request->getBody()->getContents();
    // $signQ = strtr($request->getHeader('SIGN_Q')[0], '-_', '+/');
    // $signB = strtr($request->getHeader('SIGN_B')[0], '-_', '+/');
    $uid = $request->hasHeader('UID') ? $request->getHeader('UID')[0] : $request->getQueryParam("UID");

    {
      // $sessions = array();
      // if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));
      //
      // $keyPair = [];
      // $session = array();
      // if (!empty($uid) and @array_key_exists($uid, $sessions)) {
      //     $session = $sessions[$uid];
      //     $verify = false;
      //     $signature = '';
      //     {
      //         $publicKey = strtr($session['remote.public'], '-_', '+/');
      //         // error_log(var_export(['$publicKey', $publicKey], true));
      //         $rsaPublicKey = new RSA();
      //         $rsaPublicKey->setPublicKeyFormat(RSA::PUBLIC_FORMAT_PKCS8);
      //         $rsaPublicKey->setHash('sha256');
      //         $rsaPublicKey->loadKey($publicKey);
      //         $rsaPublicKey->setSignatureMode(RSA::SIGNATURE_PKCS1);
      //         $rsaPublicKey->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
      //         // error_log(var_export(['$rsaPublicKey', $rsaPublicKey], true));
      //         if ($request->isGet()) {
      //           error_log(var_export(['SIGN_Q', $signQ], true));
      //           $signature = base64_decode($signQ);
      //         } else {
      //           error_log(var_export(['SIGN_B', $signB], true));
      //           $signature = base64_decode($signB);
      //           $verify = $rsaPublicKey->verify($bodyContents, $signature);
      //         }
      //     }
      // }
    }

    // error_log(var_export(['$request->getHeaders()', $request->getHeaders()], true));
    // error_log(var_export(['$request->getParsedBody()', $request->getParsedBody()], true));
    // error_log(var_export(['$request->getBody()->getContents()', $bodyContents], true));
    // error_log(var_export(['$verify', $verify], true));

	  $man = $request->hasHeader('Manufacturer') ? $request->getHeader('Manufacturer')[0] : '';
	  $mod = $request->hasHeader('Model') ? $request->getHeader('Model')[0] : '';

    $form = $request->getParsedBody();
    // $id_usuario = 0;
    try {
        // $usuario = $form['usuario'];
        // if (empty($usuario) == false) {
        //     $sql = "SELECT * FROM usuario WHERE nome LIKE :usuario ";
        //     $db = getConnection();
        //     $stmt = $db->prepare($sql);
        //     $stmt->bindParam(":usuario", $usuario);
        //     $stmt->execute();
        //     $usuarios = $stmt->fetchAll(PDO::FETCH_OBJ);
        //     $id_usuario = $usuarios[0]->id;
        // }

        if ($id_usuario < 1) {
            $data['error_code'] = 999;
            $data['error_description'] = "Consulta sem criterio.";
        } else {
            $db = getConnection();
            // TRAZER CEP CADASTRADO
            $cep_cadastrado = NULL;
            {
                $sql = "select resposta_marcada.texto as cep FROM questao LEFT JOIN resposta ON resposta.id_questao = questao.id  LEFT JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id  LEFT JOIN usuario ON usuario.id = resposta_marcada.id_usuario WHERE questao.tag = 'CEP' and usuario.id = :id_usuario LIMIT 1";
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id_usuario", $id_usuario);
                $stmt->execute();
                $record = $stmt->fetchAll(PDO::FETCH_OBJ);
                if (!empty($record)) $cep_cadastrado = $record[0]->cep;
            }
            // HAVENDO CEP CADASTRADO ATUALIZA COM ESTE
            if ($cep_cadastrado !== NULL) {

                try {
                    $httpClient = new \Http\Adapter\Guzzle6\Client();
                    $userAgent = "Guzzle 6";
                    $provider = \Geocoder\Provider\Nominatim\Nominatim::withOpenStreetMapServer($httpClient, $userAgent);
                    $cepResponse = cep($cep_cadastrado);
                    $geocoder = new \Geocoder\StatefulGeocoder($provider, 'en');
                    setlocale(LC_CTYPE, 'pt_BR');
                    $Coordinates = $geocoder->geocodeQuery(
                        \Geocoder\Query\GeocodeQuery::create(
                            $cepResponse->getCepModel()->logradouro . ', ' .
                            implode(' - ',[
                                $cepResponse->getCepModel()->bairro,
                                $cepResponse->getCepModel()->localidade,
                                $cepResponse->getCepModel()->uf,
                                'Brasil'
                            ])
                        )                    
                    )->first()->getCoordinates();
                    if ($Coordinates) {
                        $form['accuracy'] = 0;
                        $form['altitude'] = 0;
                        $form['latitude'] = $Coordinates->getLatitude();
                        $form['longitude'] = $Coordinates->getLongitude();
                        error_log(var_export([
                            __FILE__ => __LINE__,
                            '$id_usuario' => $id_usuario,
                            '$cep_cadastrado' => $cep_cadastrado,
                            '$form[accuracy]' => 0,
                            '$form[altitude]' => 0,
                            '$form[latitude]' => $Coordinates->getLatitude(),
                            '$form[longitude]' => $Coordinates->getLongitude(),
                        ], true));
                    }
                } catch (\Throwable $th) {
                }
            }

            $sql = "UPDATE usuario SET loc_accuracy = :loc_accuracy, loc_altitude = :loc_altitude, loc_latitude = :loc_latitude, loc_longitude = :loc_longitude WHERE id = :id_usuario;";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":loc_accuracy", $form['accuracy']);
            $stmt->bindParam(":loc_altitude", $form['altitude']);
            $stmt->bindParam(":loc_latitude", $form['latitude']);
            $stmt->bindParam(":loc_longitude", $form['longitude']);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->execute();

            $sql = "SELECT id FROM usuario_gps_track WHERE id_usuario = :id_usuario AND uid LIKE :uid ";
            if (!empty($man) and !empty($mod)) {
                $sql = $sql . " AND device_manufacturer = :manufacturer AND device_model = :model ";
            }
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->bindParam(":uid", $uid);
            if (!empty($man) and !empty($mod)) {
                $stmt->bindParam(":manufacturer", $man);
                $stmt->bindParam(":model", $mod);
            }
            $stmt->execute();
            $gps_track = $stmt->fetchAll(PDO::FETCH_OBJ);

            $id_gps_track = (empty($gps_track) ? 0 : $gps_track[0]->id);

            if ($id_gps_track > 0) {
                $dateTime = new DateTime('now');
                $sql = "UPDATE usuario_gps_track SET horario = :horario, loc_accuracy = :loc_accuracy, loc_altitude = :loc_altitude, loc_latitude = :loc_latitude, loc_longitude = :loc_longitude WHERE id = :id_gps_track;";
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":loc_accuracy", $form['accuracy']);
                $stmt->bindParam(":loc_altitude", $form['altitude']);
                $stmt->bindParam(":loc_latitude", $form['latitude']);
                $stmt->bindParam(":loc_longitude", $form['longitude']);
                $stmt->bindParam(":horario", $dateTime->format("Y-m-d H:i:s"));
                $stmt->bindParam(":id_gps_track", $id_gps_track);
                $stmt->execute();
            } else if (!empty($man) and !empty($mod)) {
                $sql = "INSERT INTO usuario_gps_track (device_manufacturer, device_model, id_usuario, uid, loc_altitude, loc_latitude, loc_longitude) VALUES (:manufacturer, :model, :id_usuario, :uid, :loc_altitude, :loc_latitude, :loc_longitude);";
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":manufacturer", $man);
                $stmt->bindParam(":model", $mod);
                $stmt->bindParam(":loc_altitude", $form['altitude']);
                $stmt->bindParam(":loc_latitude", $form['latitude']);
                $stmt->bindParam(":loc_longitude", $form['longitude']);
                $stmt->bindParam(":uid", $uid);
                $stmt->bindParam(":id_usuario", $id_usuario);
                $stmt->execute();
            } else {
                $sql = "INSERT INTO usuario_gps_track (id_usuario, uid, loc_altitude, loc_latitude, loc_longitude) VALUES (:id_usuario, :uid, :loc_altitude, :loc_latitude, :loc_longitude);";
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":loc_altitude", $form['altitude']);
                $stmt->bindParam(":loc_latitude", $form['latitude']);
                $stmt->bindParam(":loc_longitude", $form['longitude']);
                $stmt->bindParam(":uid", $uid);
                $stmt->bindParam(":id_usuario", $id_usuario);
                $stmt->execute();
            }
        }
    } catch(PDOException $e) {
	      $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    //
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
