<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/v2/usuario/login', function (Request $request, Response $response, array $args) {
  global $sql_mensagens_pesquisador, $_local_decrypt;
  $data = array(
    'error_code' => 0,
    'error_description' => 'SUCCESS',
  );
  $nome = $request->getQueryParam("nome");
  $senha = $request->getQueryParam("senha");
  $uid = null;
  $sessions = array();
  $session = array();
  try {
    // $data['debug'][] = ['$uid' => $uid, '$nome' => $nome];
    $sql =  "SELECT id, nome as login, tipo, foto_base64, tester " .
            "FROM usuario " .
            "WHERE nome LIKE :nome AND senha LIKE :senha "; // AND tipo = :tipo";
    $db = getConnection();
    $stmt = $db->prepare($sql);
    $stmt->bindParam(":nome", $nome);
    $stmt->bindParam(":senha", $senha);
    $stmt->execute();
    $usuarios = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // $data['debug'][] =  ['$nome'=> $nome, '$usuarios' => $usuarios];
    if (count($usuarios) > 0) {
      $data['usuario'] = @$usuarios[0];
      $id_usuario = $data['usuario']['id'];
      $tp_usuario = $data['usuario']['tipo'];
      if ($uid != null) {
        $session['id_usuario'] = $id_usuario;
        $session['tp_usuario'] = $tp_usuario;
        $sessions[$uid] = $session;
        @file_put_contents('sessions', serialize($sessions));
        //enviar_mensagens_pendentes_ao_usuario();
        $data['debug'][] = $pid;
      }
    }
  } catch(Exception $e) {
    $data['error_code'] = 999;
    $data['error_description'] = $e->getMessage();
  }
  // $data['debug'] = base64_encode(@var_export($data['debug'], true));
  return $response->withJson($data);
});
