<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/pesquisador/buscas/update', function (Request $request, Response $response, array $args) {

    $id_usuario = $request->getAttribute('ID_USUARIO');
    $tp_usuario = $request->getAttribute('TP_USUARIO');

    error_log(var_export([
        __FILE__ => __LINE__,
        '$id_usuario' => $id_usuario,
        '$tp_usuario' => $tp_usuario,
    ], true));

    if ($request->getAttribute('VALIDATION') == false) return $response->withStatus(403);

    // $sessions = array();
    // if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );

    $id_busca = $request->getQueryParam("idBusca");
    // $id_usuario = 0;
    // $tp_usuario = '';

    try {
        if (@empty($id_busca)) $id_busca = 0;
        $data['debug'][] = array('id_busca' => $id_busca);

        // {
        //     $uid = $request->getHeader('UID')[0];
        //     $data['debug'][] = array('uid' => $uid);
        //     $session = array();
        //     if (@array_key_exists($uid, $sessions)) {
        //         $session = $sessions[$uid];
        //         $id_usuario = $session['id_usuario'];
        //         $tp_usuario = $session['tp_usuario'];
        //     }
        //     $data['debug'][] = array('id_usuario' => $id_usuario, 'tp_usuario' => $tp_usuario);
        // }

        if ($id_usuario == 0) {
            $data['error_code'] = 999;
            $data['error_description'] = "Consulta sem criterio.";
        } else {
            $sql =
                "SELECT DISTINCT " .
                "busca.id as busca_id, " .
                "busca.nome as busca_nome, " .
                "busca._deleted as busca_deleted, " .
                "'dummy' as dummy " .
                "FROM busca  " .
                // "WHERE busca.id_usuario = :id_usuario AND ( busca._deleted = 'S' OR busca.id > :id_busca ) " .
                "WHERE busca.id_usuario = :id_usuario AND ( busca._deleted = 'S' OR busca.id > :id_busca ) " .
                ""
            ;
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->bindParam(":id_busca", $id_busca);
            $stmt->execute();
            $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);
            $data['buscas'] = $resultado;

            $data['debug'][] = array('buscas' => $data['buscas']);

            // criterios com busca associada
            $sql =
                "SELECT DISTINCT " .
                "busca.id_estudo as estudo_id, " .
                "busca.id as busca_id, " .
                "busca._deleted as busca_deleted, " .
                "resposta_buscada.id as buscada_id, " .
                "resposta_buscada.id_questao as questao_id, " .
                "resposta_buscada.id_resposta as resposta_id, " .
                "resposta_buscada.texto as buscada_texto, " .
                "resposta_buscada._deleted as buscada_deleted, " .
                "resposta.conteudo as resposta_conteudo, " .
                "resposta.texto as resposta_texto, " .
                "'dummy' as dummy " .
                "FROM busca  " .
                "INNER JOIN resposta_buscada ON resposta_buscada.id_busca = busca.id " .
                "INNER JOIN resposta ON resposta.id = resposta_buscada.id_resposta " .
                // "WHERE busca._deleted = 'N' AND ( busca.id_usuario = :id_usuario OR busca.id = :id_busca ) " .
                "WHERE busca.id_usuario = :id_usuario AND ( resposta_buscada._deleted = 'S' OR busca.id > :id_busca ) " .
                ""
            ;
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->bindParam(":id_busca", $id_busca);
            $stmt->execute();
            $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);
            $data['respostas'] = $resultado;

            $data['debug'][] = array('respostas' => $data['respostas']);

        }

    } catch(PDOException $e) {
        $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    // error_log('/pesquisador/buscas/update/ '. @var_export($data['debug'], true));
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
