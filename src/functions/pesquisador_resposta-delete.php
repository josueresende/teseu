<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->delete('/pesquisador/resposta', function (Request $request, Response $response, array $args) {

    $id_usuario = $request->getAttribute('ID_USUARIO');
    $tp_usuario = $request->getAttribute('TP_USUARIO');

    error_log(var_export([
        __FILE__ => __LINE__,
        '$id_usuario' => $id_usuario,
        '$tp_usuario' => $tp_usuario,
    ], true));

    if ($request->getAttribute('VALIDATION') == false) return $response->withStatus(403);

    // $sessions = array();
    // if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );

    $id_busca = $request->getQueryParam("idBusca");
    $id_questao = $request->getQueryParam("idQuestao");
    $id_resposta = $request->getQueryParam("idResposta");
    // $id_usuario = 0;
    // $tp_usuario = '';

    try {
        if (@empty($id_busca)) $id_busca = 0;
        $data['debug'][] = array('id_busca' => $id_busca);

        // {
        //     $uid = $request->getHeader('UID')[0];
        //     $data['debug'][] = array('uid' => $uid);
        //     $session = array();
        //     if (@array_key_exists($uid, $sessions)) {
        //         $session = $sessions[$uid];
        //         $id_usuario = $session['id_usuario'];
        //         $tp_usuario = $session['tp_usuario'];
        //     }
        //     $data['debug'][] = array('id_usuario' => $id_usuario, 'tp_usuario' => $tp_usuario);
        // }

        if ($id_usuario == 0 or $id_busca == 0 or $id_questao == 0 or $id_resposta == 0) {
            $data['error_code'] = 999;
            $data['error_description'] = "Delecao sem criterio.";
        } else {

            $sql =
                "UPDATE resposta_buscada SET _deleted = 'S'  WHERE id_busca = :id_busca AND  id_questao = :id_questao AND  id_resposta = :id_resposta " .
                ""
            ;
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_busca", $id_busca);
            $stmt->bindParam(":id_questao", $id_questao);
            $stmt->bindParam(":id_resposta", $id_resposta);
            $stmt->execute();

            publish([
              [
                'topic' => "usuario-".$id_usuario."-inbox",
                'message' => "SEARCHS_UPDATE"
              ]
            ]);
        }

    } catch(PDOException $e) {
        $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);

});
