<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/usuario/login', function (Request $request, Response $response, array $args) {
    global $sql_mensagens_pesquisador, $_local_decrypt;
    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );

    $nome = $request->getQueryParam("nome");
    $senha = $request->getQueryParam("senha");
    $id_ultima_mensagem = $request->getQueryParam("id_ultima_mensagem");

    $uid = null;
    $sessions = array();
    $session = array();

    try {
        if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));

        $uid = $request->hasHeader('UID') ? $request->getHeader('UID')[0] : $request->getQueryParam("UID");
        $usr = $request->getQueryParam("username");
        $pwd = $request->getQueryParam("password");

        if (@array_key_exists($uid, $sessions)) {
            $session = $sessions[$uid];
            $data['debug'][] = ['$session' => $session];
        }

	      if (!empty($uid) and !empty($usr) and !empty(pwd)) {
            if (has_session($uid) == false) {
                    $data['error_code'] = 205;
                    $data['error_description'] = 'Sessao nao iniciado, recomece';
      	    }
      	    $data['debug'][] = ['$usr' => $usr, '$pwd' => $pwd];
            $nome = local_decrypt($uid, $usr);
      	    $senha = local_decrypt($uid, $pwd);
      	    $data['debug'][] = ['local_decrypt' => $_local_decrypt];
        }

        $data['debug'][] = ['$uid' => $uid, '$nome' => $nome];

        $sql = "SELECT id, nome as login, tipo, foto_base64, tester
                FROM usuario
                WHERE _enabled = 'S' AND (usuario.login LIKE :login_plain OR usuario.login LIKE :login_crypto)  AND senha LIKE :senha "; // AND tipo = :tipo";
        $db = getConnection();
        $stmt = $db->prepare($sql);

        $stmt->bindParam(":login_plain", $nome);
        $stmt->bindParam(":login_crypto", md5(sha1($nome)));
        $stmt->bindParam(":senha", md5(md5($senha)));

        $stmt->execute();

        $usuarios = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $data['debug'][] =  ['$nome'=> $nome, '$usuarios' => $usuarios];

        if (count($usuarios) > 0) {
            $data['usuario'] = @$usuarios[0];
            $id_usuario = $data['usuario']['id'];
            $tp_usuario = $data['usuario']['tipo'];

            if ($uid != null) {
                $session['id_usuario'] = $id_usuario;
                $session['tp_usuario'] = $tp_usuario;
                $sessions[$uid] = $session;
                @file_put_contents('sessions', serialize($sessions));
            }
/* // */
            // $pid = pcntl_fork();
            // $child = ($pid === 0);
/* // */
            $sql =
                "SELECT DISTINCT " .
                    "questao.id as questao_id, " .
                    "questao.tag as questao_tag, " .
                    "questao.limite_marcadas as questao_limite_marcadas, " .
                    "resposta.id as resposta_id, " .
                    "resposta.tag as resposta_tag, " .
                    "resposta.conteudo as resposta_conteudo, " .
                    "resposta.texto as resposta_texto, " .
                    "resposta_marcada.id as marcada_id, " .
                    "resposta_marcada.texto as marcada_texto, " .
                    "'dummy' as dummy " .
                    "FROM questao " .
                    "LEFT JOIN resposta ON resposta.id_questao = questao.id " .
                    "LEFT JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id " .
                    "LEFT JOIN usuario ON usuario.id = resposta_marcada.id_usuario " .
                    "WHERE usuario.id = :id_usuario " .
                    "AND questao.tag IS NOT NULL " .
                    "ORDER BY questao.id,resposta.id "
            ;
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->execute();
            $resultados = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $data['debug'][] = ['$resultados' => $resultados];
            foreach ($resultados as $resultado) {
                $usuario_property = strtolower($resultado['questao_tag']);
                //
                if ($resultado['questao_limite_marcadas'] == 1) {
                    if ($resultado['resposta_tag'] == null) {
                        $data['usuario'][$usuario_property] = $resultado['marcada_texto'];
                    } else {
                        $data['usuario'][$usuario_property] = $resultado['resposta_tag'];
                    }
                } else if ($resultado['questao_limite_marcadas'] == -1) {
                    if ($resultado['resposta_tag'] == null) {
                        $data['usuario'][$usuario_property][] = $resultado['marcada_texto'];
                    } else {
                        $item_property = strtolower($resultado['resposta_tag']);
                        $data['usuario'][$usuario_property][$item_property] = $resultado['marcada_texto'];
                    }
                }
            }
            $data['debug'][] = ['$data[usuario]' => $data['usuario']];
/* // */
            if ($tp_usuario == "VOLUNTARIO") {
/* // */
/*
                $sql =
                    "SELECT DISTINCT " .
                    "formulario.id as formulario_id, " .
                    "formulario.titulo as formulario_titulo, " .
                    "formulario.resposta_texto_livre as formulario_texto," .
                    "resposta_referencia.id as resposta_referencia_id, " .
                    "resposta_referencia.id_questao as questao_referencia_id, " .
                    "'dummy' as dummy " .
                    "FROM formulario  " .
                    "INNER JOIN questao_formulario ON questao_formulario.id_formulario = formulario.id " .
                    "INNER JOIN questao ON questao.id = questao_formulario.id_questao " .
                    "LEFT JOIN resposta ON resposta.id_questao = questao.id " .
                    "LEFT JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id ".
                              "AND resposta_marcada.id_usuario IN (SELECT id FROM usuario WHERE usuario.nome = :usuario) " .
                    "LEFT JOIN resposta resposta_referencia ON resposta_referencia.id = formulario.id_resposta_referencia " .
    //                "WHERE formulario.id_resposta_referencia IS NULL " .
    //                "OR formulario.id_resposta_referencia IN ".
    //                "( SELECT id_resposta FROM resposta_marcada INNER JOIN usuario ON usuario.id = resposta_marcada.id_usuario WHERE usuario.nome = :usuario ) ".
                     "ORDER BY formulario.id "
                ;
                $db = getConnection();
                $stmt = $db->prepare($sql);

                $stmt->bindParam(":usuario", $nome);

                $stmt->execute();

                $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);

                $data['formularios'] = $resultado;
/* // */
/*
                $sql =
                    "SELECT DISTINCT " .
                    "formulario.id as formulario_id, " .
                    "questao.id as questao_id, " .
                    "questao.tag as questao_tag, " .
                    "questao.texto as questao_texto, " .
		                "questao.limite_marcadas as questao_limite_marcadas, " .
                    "questao_formulario.ordem_exibicao as ordem_exibicao, ".
                    "'dummy' as dummy " .
                    "FROM formulario  " .
                    "INNER JOIN questao_formulario ON questao_formulario.id_formulario = formulario.id " .
                    "INNER JOIN questao ON questao.id = questao_formulario.id_questao " .
                    "LEFT JOIN resposta ON resposta.id_questao = questao.id " .
                    "LEFT JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id ".
                              "AND resposta_marcada.id_usuario IN (SELECT id FROM usuario WHERE usuario.nome = :usuario) " .
//                    "WHERE formulario.id_resposta_referencia IS NULL " .
//                    "OR formulario.id_resposta_referencia IN ".
//                    "( SELECT id_resposta FROM resposta_marcada INNER JOIN usuario ON usuario.id = resposta_marcada.id_usuario WHERE usuario.nome = :usuario ) ".
//                    "ORDER BY formulario.id,questao.id,resposta.id ".
                    "ORDER BY questao_formulario.ordem_exibicao ".
                    ""
                ;
                $db = getConnection();
                $stmt = $db->prepare($sql);

                $stmt->bindParam(":usuario", $nome);

                $stmt->execute();

                $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);

                $data['questoes'] = $resultado;
/* // */
                $sql =
                    "SELECT DISTINCT " .
                    "formulario.id as formulario_id, " .
                    "questao.id as questao_id, " .
                    // "questao.tag as questao_tag, " .
                    "resposta.id as resposta_id, " .
                    // "resposta.conteudo as resposta_conteudo, " .
                    // "resposta.texto as resposta_texto, " .
                    // "resposta.tag as resposta_tag, " .
                    "resposta_marcada.id as marcada_id, " .
                    "resposta_marcada.texto as marcada_texto, " .
                    "questao_formulario.ordem_exibicao as ordem_exibicao, ".
                    "'dummy' as dummy " .
                    "FROM formulario  " .
                    "INNER JOIN questao_formulario ON questao_formulario.id_formulario = formulario.id " .
                    "INNER JOIN questao ON questao.id = questao_formulario.id_questao " .
                    "LEFT JOIN resposta ON resposta.id_questao = questao.id " .
                    "INNER JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id ".
                        "AND resposta_marcada.id_usuario = :id_usuario " .
//                    "WHERE formulario.id_resposta_referencia IS NULL " .
//                    "OR formulario.id_resposta_referencia IN ".
//                    "( SELECT id_resposta FROM resposta_marcada INNER JOIN usuario ON usuario.id = resposta_marcada.id_usuario WHERE usuario.nome = :usuario ) ".
//                    "ORDER BY formulario.id,questao.id,resposta.id ".
                    "ORDER BY questao_formulario.ordem_exibicao ".
                    ""
                ;
                $db = getConnection();
                $stmt = $db->prepare($sql);

                // $stmt->bindParam(":login", md5(sha1($nome)));
                $stmt->bindParam(":id_usuario", $id_usuario);

                $stmt->execute();

                $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);

                error_log(var_export(['sql' => $sql, '$nome' => $nome, 'resultado' => $resultado], true));

                $data['respostas'] = $resultado;
/* // */
            }
/* // */
            if ($tp_usuario == "PESQUISADOR") {
                $sql =
                    "SELECT DISTINCT " .
                    "busca.id as busca_id, " .
                    "busca.nome as busca_nome, " .
                    "'dummy' as dummy " .
                    "FROM busca  " .
                    "WHERE busca._deleted = 'N' AND busca.id_usuario = :id_usuario " .
                    ""
                ;
                $db = getConnection();
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id_usuario", $id_usuario);
                $stmt->execute();
                $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $data['buscas'] = $resultado;

                $db = getConnection();
                $stmt = $db->prepare($sql_mensagens_pesquisador);
                $stmt->bindParam(":id_usuario", $id_usuario);
                $stmt->execute();
                $mensagens = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $data['mensagens'] = $mensagens;
                $data['debug'][] = ['$mensagens' => $mensagens];
            }
/* // */
        } else if ($data['error_code'] == 0) {
            $data['error_code'] = 999;
            $data['error_description'] = "Usuário não autenticado.";
        }
    } catch(Exception $e) {
        $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
