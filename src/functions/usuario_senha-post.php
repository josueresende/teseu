<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->post('/usuario/senha', function (Request $request, Response $response, array $args) {
    $sessions = array();
    if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));
    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    $form = $request->getParsedBody();
    $id_usuario = 0;
    $tp_usuario = '';
    $password   = '';
    {
        $uid = $request->hasHeader('UID') ? $request->getHeader('UID')[0] : $request->getQueryParam("UID");
        $pwd = $form["password"];

        if (!empty($uid) and !empty(pwd)) {
            $password = local_decrypt($uid, $pwd);
        }

        $data['debug'][] = array('uid' => $uid);
        $session = array();
        if (@array_key_exists($uid, $sessions)) {
            $session = $sessions[$uid];
            $id_usuario = $session['id_usuario'];
            $tp_usuario = $session['tp_usuario'];
        }
        $data['debug'][] = array('id_usuario' => $id_usuario);
    }
    try {
        if ($id_usuario == 0 || empty($password)) {
            $data['error_code'] = 999;
            $data['error_description'] = "Consulta sem criterio.";
        } else {
            $sql = "UPDATE usuario SET senha = :senha WHERE id = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":senha", md5(md5($password)));
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->execute();
        }
    } catch(PDOException $e) {
	    $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
