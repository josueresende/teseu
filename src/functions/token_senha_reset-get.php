<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

function formPasswordResetOk() { ?>
<!DOCTYPE html>
<html>
 <head>
   <meta charset="UTF-8">
   <title></title>
   <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
   <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link href="/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
 </head>
 <body class="login-page">
   <div class="login-box">
     <div class="login-logo">
       <!-- Login  -->
     </div><!-- /.login-logo -->
     <div class="login-box-body">
     	<img src="/images/revolution_logo.png" width="320px;"><br/><br/>
       <div class="row">
           <div class="col-md-12">
           </div>
       </div>
       <div class="row">
           <div class="col-md-12">
             Sua senha foi atualizada.
           </div>
       </div>
     </div><!-- /.login-box-body -->
   </div><!-- /.login-box -->
   <script src="/js/jQuery-2.1.4.min.js"></script>
   <script src="/js/bootstrap.min.js" type="text/javascript"></script>
 </body>
</html>
<?php }

function formPasswordReset($config) { ?>
<!DOCTYPE html>
<html>
 <head>
   <meta charset="UTF-8">
   <title></title>
   <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
   <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
   <link href="/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
 </head>
 <body class="login-page">
   <div class="login-box">
     <div class="login-logo">
       <!-- Login  -->
     </div><!-- /.login-logo -->
     <div class="login-box-body">
     	<img src="/images/revolution_logo.png" width="320px;"><br/><br/>
       <div class="row">
           <div class="col-md-12">
           </div>
       </div>
       <form action="/token/senha/reset/<?php echo $config['hash']; ?>" method="post">
         <div class="form-group has-feedback">
           <?php if ($config['email-invalido']) : ?>
            <div class="input row-input with-error has-error text-red">O e-mail está incorreto.</div>
           <?php endif; ?>
           <input type="text" class="form-control" placeholder="E-mail" name="email" required value="<?php echo $config['email']; ?>"  />
           <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
         </div>
         <div class="form-group has-feedback">
           <input type="password" class="form-control" placeholder="Password" name="password" required />
           <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         </div>
         <div class="form-group has-feedback">
           <?php if ($config['password-invalido']) : ?>
            <div class="input row-input with-error has-error text-red">As senhas não estão iguais, tente novamente.</div>
           <?php endif; ?>
           <input type="password" class="form-control" placeholder="Confirm Password" name="password-confirm" required />
           <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         </div>
         <div class="row">
           <div class="col-xs-8">
             <!-- <div class="checkbox icheck">
               <label>
                 <input type="checkbox"> Remember Me
               </label>
             </div>  -->
           </div><!-- /.col -->
           <div class="col-xs-8">
             <input type="submit" class="btn btn-primary btn-block btn-flat" value="ATUALIZAR SENHA" />
           </div><!-- /.col -->
         </div>
       </form>
     </div><!-- /.login-box-body -->
   </div><!-- /.login-box -->
   <script src="/js/jQuery-2.1.4.min.js"></script>
   <script src="/js/bootstrap.min.js" type="text/javascript"></script>
 </body>
</html>
<?php }

$app->post('/token/senha/reset/{hash}', function (Request $request, Response $response, array $args) {
    $form = $request->getParsedBody();
    try {
        $sql =  "SELECT token.id_usuario, token.acao, token.hash " .
                "FROM token " .
                // "INNER JOIN usuario ON usuario.id = token.id_usuario " .
                "WHERE token.acao = 'PASSWORD_RESET' AND token.hash = :hash " .
                "AND now() < token.data_delecao ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":hash", $args['hash']);
        $stmt->execute();
        $tokens = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($tokens) == 0) return $response->withStatus(403);
        $token = $tokens[0];
        $data['debug'][] = ['$tokens' => $tokens, '$token' => $token];
    } catch (Exception $e) {
        //
    }

    $parametros = ['hash' => $args['hash'],'email' => $form['email']];

    error_log(var_export($parametros, true));

    if ($form['password'] != $form['password-confirm']) $parametros['password-invalido'] = true;

    if (filter_var($form['email'], FILTER_VALIDATE_EMAIL) != $form['email']) $parametros['email-invalido'] = true;

    error_log(var_export($parametros, true));

    if (array_key_exists('email-invalido', $parametros) || array_key_exists('password-invalido', $parametros)) {
        formPasswordReset($parametros);
        return $response->withStatus(200);
    }

    try {
        $sql =  "UPDATE token SET token.data_delecao = now() WHERE token.hash = :hash ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":hash", $token['hash']);
        $stmt->execute();

        $senha = md5(md5($form['password']));

        $sql =  "UPDATE usuario SET usuario._enabled = 'S', usuario.senha = :senha WHERE usuario.id = :id_usuario ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":senha", $senha);
        $stmt->bindParam(":id_usuario", $token['id_usuario']);
        $stmt->execute();

        formPasswordResetOk();

    } catch (Exception $e) {
        //
    }

    return $response->withStatus(200);
});

$app->get('/token/senha/reset/{hash}', function (Request $request, Response $response, array $args) {
    $data['debug'][] = ['$args' => $args];
    try {
      $sql =  "SELECT token.id_usuario, token.acao " .
              "FROM token " .
              "WHERE token.acao = 'PASSWORD_RESET' AND token.hash = :hash " .
              "AND now() < token.data_delecao ";
      $db = getConnection();
      $stmt = $db->prepare($sql);
      $stmt->bindParam(":hash", $args['hash']);
      $stmt->execute();
      $tokens = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($tokens) == 0) return $response->withStatus(403);
      $token = $tokens[0];
      $data['debug'][] = ['$tokens' => $tokens, '$token' => $token];
      formPasswordReset($args);
    } catch (Exception $e) {

    }
    return $response->withStatus(200);
});
