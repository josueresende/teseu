<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/policy', function (Request $request, Response $response, array $args) {
    return $response->write(file_get_contents(dirname(__FILE__).'/mobile-privacy-policy-geolocated-apps.html'));
});
