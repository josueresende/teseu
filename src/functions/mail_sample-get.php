<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/mail/sample', function (Request $request, Response $response, array $args) {
    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
	$data['debug']['mailer'] = mailer(array('josue.resende@gmail.com'), "Teste Assunto", "Teste Corpo");
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
