<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->post('/pesquisador/busca', function (Request $request, Response $response, array $args) {
    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    $form = $request->getParsedBody();
    try {
        $sql = "SELECT * FROM usuario WHERE nome LIKE :usuario ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":usuario", $form['usuario']);
        $stmt->execute();

        $usuarios = $stmt->fetchAll(PDO::FETCH_OBJ);

        $data['debug'] = array(@var_export($usuarios, true),@var_export($stmt, true),@var_export($form, true));

        if ($usuarios == null) {
            $data['error_code'] = 999;
            $data['error_description'] = "Usuário não encontrado.";
        } else {
            $id_usuario = $usuarios[0]->id;
            $sql = "INSERT INTO busca (id_usuario, nome) VALUES (:id_usuario, :nome);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $id_usuario);
            $stmt->bindParam(":nome", $form['nome']);
            $stmt->execute();
            publish([
              [
                'topic' => "usuario-".$id_usuario."-inbox",
                'message' => "SEARCHS_UPDATE"
              ]
            ]);
        }
// */
    } catch(PDOException $e) {
	    $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
// */
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
