<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->get('/pesquisador/formularios', function (Request $request, Response $response, array $args) {

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
    );
    $id_usuario = -1;
    $id_versao = -1;
    try {
        $id_versao_formulario = $request->getQueryParam("idVersaoFormulario");
        $id_versao_questao_formulario = $request->getQueryParam("idVersaoQuestaoFormulario");
        if ($id_usuario == 0 and $id_busca == 0) {
            $data['error_code'] = 999;
            $data['error_description'] = "Consulta sem criterio.";
        } else if (empty($id_versao_formulario) || empty($id_versao_questao_formulario)) {
            $data['error_code'] = 999;
            $data['error_description'] = "Versão vazia.";
        } else {
            {
                $sql =
                    "SELECT DISTINCT " .
                    "formulario.id as formulario_id, " .
                    "formulario.id_resposta_referencia as formulario_id_resposta, " .
                    "formulario.titulo as formulario_titulo, " .
                    "formulario.id_versao as _versao, " .
                    "'dummy' as dummy " .
                    "FROM formulario " .
                    "WHERE formulario.id_versao > :id_versao "
                ;
                $db = getConnection();
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id_versao", $id_versao_formulario);
                $stmt->execute();
                $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);
                $data['formularios'] = $resultado;
            }
            {
                $sql =
                    "SELECT DISTINCT " .
                    "questao_formulario.id as questaoformulario_id, " .
                    "questao_formulario.id_formulario as questaoformulario_id_formulario, " .
                    "questao_formulario.id_questao as questaoformulario_id_questao, " .
                    "questao_formulario.ordem_exibicao as questaoformulario_ordem_exibicao, " .
                    "questao_formulario.id_versao as _versao, " .
                    "'dummy' as dummy " .
                    "FROM questao_formulario " .
                    "WHERE questao_formulario.id_versao > :id_versao ".
                    "ORDER BY questao_formulario.ordem_exibicao ".
                    ""
                ;
                $db = getConnection();
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id_versao", $id_versao_questao_formulario);
                $stmt->execute();
                $resultado = $stmt->fetchAll(PDO::FETCH_OBJ);
                $data['questoesformularios'] = $resultado;
            }
        }

//        $data['debug'] = array(@var_export($usuarios[0], true),@var_export($stmt, true),@var_export($form, true));

    } catch(PDOException $e) {
	     $data['error_code'] = 999;
       $data['error_description'] = $e->getMessage();
    }
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
