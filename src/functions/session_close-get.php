<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$_session_open = [];
$app->get('/session/close', function (Request $request, Response $response, array $args) {
    global $_session_open;
    $sessions = array();
    if ($request->hasHeader('UID')) {
        try {
            if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));
            error_log(var_export(['$sessions keys', array_keys($sessions)], true));
            $data = array(
              'error_code' => 0,
              'error_description' => 'SUCCESS',
              // 'debug' => [['$sessions' => $sessions]],
            );
            $uid = $request->getHeader('UID')[0];
            $data['uid'] = $uid;
            if (@array_key_exists($uid, $sessions)) {
                unset($sessions[$uid]);
                @file_put_contents('sessions', serialize($sessions));
            }

        } catch(Exception $e) {
            // $data['debug']['Exception $e'] = $e->getMessage();
            error_log(var_export(['Exception $e', $e], true));
        }
    } else {
        $data = array(
          'error_code' => 999,
          'error_description' => 'Sem cabecalho de identificação',
        );
    }
    error_log(var_export(['$data', $data], true));
    // $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
