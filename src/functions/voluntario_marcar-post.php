<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'sql_pack.php';

$app->post('/voluntario/marcar', function (Request $request, Response $response, array $args) {
    $sessions = array();
    if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));

    $data = array(
        'error_code' => 0,
        'error_description' => 'SUCCESS',
        'debug' => array('uri' => $request->getUri(), 'parsedbody' => $request->getParsedBody()),
    );

    $id_usuario = 0;
    $tp_usuario = '';

    $form = $request->getParsedBody();
    try {

        {
            $uid = $request->getHeader('UID')[0];
            $data['debug'][] = array('uid' => $uid);
            $session = array();
            if (@array_key_exists($uid, $sessions)) {
                $session = $sessions[$uid];
                $id_usuario = $session['id_usuario'];
                $tp_usuario = $session['tp_usuario'];
            }
            $data['debug'][] = array('id_usuario' => $id_usuario, 'tp_usuario' => $tp_usuario);
        }

        if ($id_usuario == 0) {
            $data['error_code'] = 999;
            $data['error_description'] = "Usuário inexistente.";
        } else {

            $sql =
                "SELECT DISTINCT " .
                "questao.id as questao_id, " .
                "questao.texto as questao_texto, " .
                "questao.texto_especifico as questao_especifico, " .
                "questao.is_criterio as questao_iscriterio, " .
                "questao.tag as questao_tag, " .
                "questao.config as questao_config, " .
                "questao.limite_marcadas as questao_limite_marcadas, " .
                "questao.id_versao as _versao, " .
                "'dummy' as dummy " .
                "FROM questao " .
                "WHERE id = :id_questao "
            ;
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_questao", $form['idQuestao']);
            $stmt->execute();
            $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $questoes = $resultado;

            $data['debug']['questoes'] = $questoes;

            $questao = $questoes[0];

            $data['debug']['questao'] = $questao;

            if (empty($questao['questao_tag']) == false) {
                $sql = "DELETE FROM resposta_marcada WHERE id_usuario = :id_usuario AND ".
                    "id_questao IN (SELECT id FROM questao WHERE limite_marcadas = 1 AND tag = :tag_questao)";
                $db = getConnection();
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id_usuario", $id_usuario);
                $stmt->bindParam(":tag_questao", $questao['questao_tag']);
                $stmt->execute();
            }

            {
                $sql = "DELETE FROM resposta_marcada WHERE id_usuario = :id_usuario AND id_questao = :id_questao AND ".
                       "id_questao IN (SELECT id FROM questao WHERE limite_marcadas = 1)";
                $db = getConnection();
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id_usuario", $id_usuario);
                $stmt->bindParam(":id_questao", $form['idQuestao']);
                $stmt->execute();
            }

            {
                $sql = "DELETE FROM resposta_marcada WHERE id_usuario = :id_usuario AND id_questao = :id_questao AND id_resposta = :id_resposta AND ".
                       "id_questao IN (SELECT id FROM questao WHERE limite_marcadas = -1) ";
                $db = getConnection();
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id_usuario", $id_usuario);
                $stmt->bindParam(":id_questao", $form['idQuestao']);
                $stmt->bindParam(":id_resposta", $form['idResposta']);
                $stmt->execute();
            }

            if ($form['idResposta'] != 0) {
                $sql = "INSERT INTO resposta_marcada (id_questao, id_resposta, id_usuario, texto) VALUES (:id_questao, :id_resposta, :id_usuario, :texto);";
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id_questao", $form['idQuestao']);
                $stmt->bindParam(":id_resposta", $form['idResposta']);
                $stmt->bindParam(":id_usuario", $id_usuario);
                $stmt->bindParam(":texto", $form['texto']);
                $stmt->execute();
            }
            {
                $sql =
                    "SELECT resposta_marcada.id " .
                    "FROM resposta_marcada " .
                    "INNER JOIN questao ON questao.id = resposta_marcada.id_questao " .
                    "INNER JOIN questao_formulario ON questao_formulario.id_questao = resposta_marcada.id_questao " .
                    "INNER JOIN formulario ON formulario.id = questao_formulario.id_formulario " .
                    "WHERE resposta_marcada.id_usuario = :id_usuario ".
                    "AND formulario.id_resposta_referencia NOT IN ".
                    "(SELECT resposta_marcada.id_resposta FROM resposta_marcada WHERE resposta_marcada.id_usuario = :id_usuario)"
                ;
                $db = getConnection();
                $stmt = $db->prepare($sql);
                $stmt->bindParam(":id_usuario", $id_usuario);
                $stmt->execute();
                $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if ($resultado != NULL) error_log("removendo ".count($resultado)." respostas sem vinculo");
                foreach ($resultado as $key => $value) {
                    $sql = "DELETE FROM resposta_marcada WHERE id_usuario = :id_usuario AND id = :id";
                    $db = getConnection();
                    $stmt = $db->prepare($sql);
                    $stmt->bindParam(":id", $value['id']);
                    $stmt->bindParam(":id_usuario", $id_usuario);
                    $stmt->execute();
                }
            }
            // $sql = "DELETE FROM resposta_marcada WHERE id_usuario = :id_usuario AND ".
            //        "resposta_marcada.id_resposta NOT IN (".
            //            "SELECT formulario.id_resposta_referencia ".
            //            "FROM resposta_marcada ".
            //            "INNER JOIN questao_formulario ON questao_formulario.id_questao = resposta_marcada.id_questao ".
            //            "INNER JOIN formulario ON formulario.id = questao_formulario.id_formulario ".
            //            "WHERE formulario.id_resposta_referencia IS NOT NULL ".
            //            "AND resposta_marcada.id_usuario = :id_usuario ".
            //        ")";
            //
            // $db = getConnection();
            // $stmt = $db->prepare($sql);
            // $stmt->bindParam(":id_usuario", $id_usuario);
            // $stmt->execute();
        }
// */
    } catch(PDOException $e) {
	      $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }
// */
    // error_log(var_export($data['debug'], true));
    $data['debug'] = base64_encode(@var_export($data['debug'], true));
    return $response->withJson($data);
});
