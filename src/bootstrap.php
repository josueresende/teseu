<?php

// Instantiate the app
require __DIR__ . '/../vendor/autoload.php';

/**
 * Configurações
 */
$configs = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

function getConnection() {
    $dbhost="mysql";
    $dbuser="teseu";
    $dbpass="teseu";
    $dbname="teseu";
    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}

$container = new \Slim\Container($configs);

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

//error_log(var_export([
//    __FILE__ => __LINE__,
//    '$container' => $container,
//], true));

/**
 * Token JWT
 */
$container['database-connection'] = getConnection();
$container['secretkey'] = md5(
    'Syadavaktavya Syadasti Syannasti Syadasti Cavaktavyasca Syadasti Syannasti Syadavatavyasca Syadasti Syannasti Syadavaktavyasca', 
    true
);

/**
 * Converte os Exceptions Genéricas dentro da Aplicação em respostas JSON
 */
$container['errorHandler'] = function ($container) {
    return function ($request, $response, $exception) use ($container) {
        $statusCode = $exception->getCode() ? $exception->getCode() : 500;
        error_log(var_export([
            __FILE__ => __LINE__,
            '$statusCode' => $statusCode,
            '$exception->getMessage()' => $exception->getMessage(),
        ], true));
        return $container['response']->withStatus($statusCode)
            ->withHeader('Content-Type', 'Application/json')
            ->withJson(["message" => $exception->getMessage()], $statusCode);
    };
};
/**
 * Converte os Exceptions de Erros 405 - Not Allowed
 */
$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        return $c['response']
            ->withStatus(405)
            ->withHeader('Allow', implode(', ', $methods))
            ->withHeader('Content-Type', 'Application/json')
            ->withHeader("Access-Control-Allow-Methods", implode(",", $methods))
            ->withJson(["message" => "Method not Allowed; Method must be one of: " . implode(', ', $methods)], 405);
    };
};
/**
 * Converte os Exceptions de Erros 404 - Not Found
 */
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        return $container['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'Application/json')
            ->withJson(['message' => 'Page not found']);
    };
};

$app = new \Slim\App($container);

// Register middleware
require __DIR__ . '/middleware.php';

/**
 * @Middleware Tratamento da / do Request
 * true - Adiciona a / no final da URL
 * false - Remove a / no final da URL
 */
$app->add(new \Psr7Middlewares\Middleware\TrailingSlash(false));

/**
 * Auth básica do JWT
 * Whitelist - Bloqueia tudo, e só libera os itens dentro do "ignore"
 * curl -X POST http://teseu.pasifae.name/phaedra/token -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"username":"someuser","password":"somepassword"}'
 */
$app->add(new \Tuupola\Middleware\JwtAuthentication([
    "regexp" => "/(.*)/", //Regex para encontrar o Token nos Headers - Livre
    // "header" => "X-Token", //O Header que vai conter o token
    "path" => ["/phaedra"], //Vamos cobrir toda a API a partir do /
    "ignore" => ["/phaedra/token", "/phaedra/register", "/phaedra/reset"], //Vamos adicionar a exceção de cobertura a rota /auth
    "realm" => "Protected",
    "secret" => $container['secretkey'], //Nosso secretkey criado
//    "error" => function ($response, $arguments) {
//        $data["status"] = "error";
//        $data["message"] = $arguments["message"];
//        error_log(var_export([
//            __FILE__ => __LINE__,
//            '$arguments' => $arguments,
//        ], true));
//        return $response
//            ->withHeader("Content-Type", "application/json");
//    },
    "error" => function ($response, $arguments) {
        $data["status"] = "error";
        $data["message"] = $arguments["message"];
        error_log(var_export([
            __FILE__ => __LINE__,
            '$data' => $data,
        ], true));
        return $response
            ->withHeader("Content-Type", "application/json")
            ->getBody()
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    },
    "before" => function ($request, $arguments) {
        $payload = [
            'userid' > 0,
        ];

        try {

            $payload = openssl_decrypt(
                $arguments['decoded']['payload'], 
                'aes256', 
                sha1($container['secretkey'], true)
            );

            $payload = json_decode($payload);

            foreach ($payload as $key => $value) {
                $key = preg_replace('/[0-9]+/', '', $key);
                if (strlen($key) == 0) continue;
                $request = $request->withAttribute($key, $value);
            }

        } catch (\Throwable $th) {
            error_log(var_export([
                __FILE__ => __LINE__,
                '__STATS__' => 'before',
                '$th' => $th,
            ], true));
        }
        
        return $request;
    },
    "secure" => false,
]));

// Register routes
require __DIR__ . '/routes.php';
