<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;

foreach (['phaedra/auth', 'phaedra/profile', 'phaedra/forms', 'functions'] as $subdir) {
    $directory = dirname(__FILE__).'/'.$subdir;
    $directory_scan = scandir($directory);
    foreach ($directory_scan as $directory_scanned_item) {
        $directory_item = $directory . '/' . $directory_scanned_item;
        if (is_dir($directory_item)) continue;
        if (!preg_match("/^.*\.(php)$/i", $directory_scanned_item)) continue;
        // echo '<pre>'.var_export($directory_item, true).'</pre>';
//        error_log(var_export([
//            __FILE__ => __LINE__,
//            '$directory_item' => $directory_item,
//        ], true));
        include_once $directory_item;
    }
}

//*
$_publish = [];
function publish(array $messages = []) {
   global $_publish;
   $mqttClient = new phpMQTT("teseu.pasifae.name", 1883, "Slim");
   // $_publish[] = ['messages' => $messages, 'phpMQTT' => $mqttClient];
   if ($mqttClient->connect(false)) {
      foreach($messages as $message) {
         $_publish[] = ['message' => $message];
         error_log(var_export([$message['topic'], $message['message']], true));
         $mqttClient->publish($message['topic'], $message['message'], 1);
      }
      $mqttClient->close();
      $mqttClient->disconnect();
   } else {
      $_publish[] = ['connect' => 'fail'];
   }
}
//
$app->get('/tests/cep2gps/{cep}', function (Request $request, Response $response, array $args) {
  // $data['debug'][] = ['$request' => $request, '$response' => $response, '$args' => $args];
  $httpClient = new \Http\Adapter\Guzzle6\Client();
  $userAgent = "Guzzle 6";
  $provider = \Geocoder\Provider\Nominatim\Nominatim::withOpenStreetMapServer($httpClient, $userAgent);
  $cepResponse = cep($args['cep']);
  $data['cep-response'] = $cepResponse->getCepModel();
  $geocoder = new \Geocoder\StatefulGeocoder($provider, 'en');
  setlocale(LC_CTYPE, 'pt_BR');
  $data['geo-query'] = $data['cep-response']->logradouro . ', ' .
  implode(' - ',[
    $data['cep-response']->bairro,
    // Normalizer::normalize($data['cep-response']->localidade, Normalizer::FORM_C),
    $data['cep-response']->localidade,
    $data['cep-response']->uf,
    'Brasil'
  ]);
  // $data['geo-query'] = iconv("UTF-8", "ASCII//TRANSLIT", $data['geo-query']);
  // $data['geo-query'] = preg_replace("[^\w\s]", "", iconv("UTF-8", "ASCII//TRANSLIT", $data['geo-query']));
  // $data['geo-query'] = '10 rue Gambetta, Paris, France';
  $data['create-query'] = \Geocoder\Query\GeocodeQuery::create($data['geo-query']);
  // $data['geo-response'] = $geocoder->geocodeQuery($data['create-query'])->first();
  $data['latitude'] = $geocoder->geocodeQuery($data['create-query'])->first()->getCoordinates()->getLatitude();
  $data['longitude'] = $geocoder->geocodeQuery($data['create-query'])->first()->getCoordinates()->getLongitude();
  // $data['geo-response-var-export'] = '<pre>'.var_export($data['geo-response'], TRUE).'</pre>';
  // $data['geo-response'][1] = $geocoder->geocode(
  //   $data['geo-query']
  // );
  // return $response->withText($data);
  return '<pre>'.var_export($data, TRUE).'</pre>';
});
//
$_test_mqtt_publish = [];
$app->get('/tests/mqtt/publish/{topic}/{message}', function (Request $request, Response $response, array $args) {
   global $_test_mqtt_publish, $_publish;
   $data['debug'][] = ['$request' => $request, '$response' => $response, '$args' => $args];
   $message = [
      'topic' => $args['topic'],
      'message' => $args['message']
   ];
   publish([$message]);
   $data['debug'][] = ['publish' => $_publish];
   //$data['debug'] = base64_encode(@var_export($data['debug'], true));
   return $response->withJson($data);
});
//  */
$app->get('/tests/post/email/{subject}/{content}', function (Request $request, Response $response, array $args) {
	mailer('josue.resende@icict.fiocruz.br', $args['subject'], $args['content'].getenv('MAIL_HOST'));
});
// */
function mailer($para, $assunto, $corpo) {
	$mail = new PHPMailer(false);                              // Passing `true` enables exceptions
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp.googlemail.com';                  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'ariadne.mobileapp@gmail.com';      // SMTP username
		$mail->Password = 'p451f43-m1n05';                    // SMTP password
		$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 465;                                    // TCP port to connect to

		//Recipients
		$mail->setFrom('ariadne.mobileapp@gmail.com', 'Mobile App');
		$mail->addAddress($para, $para);     // Add a recipient
		//$mail->addAddress('ellen@example.com');               // Name is optional
		//$mail->addReplyTo('info@example.com', 'Information');
		//$mail->addCC('cc@example.com');
		//$mail->addBCC('bcc@example.com');

		//Attachments
		//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

		//Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = '=?UTF-8?B?'.base64_encode($assunto).'?=';
		$mail->Body    = $corpo;
		$mail->AltBody = $corpo;

		$mail->send();
	} catch (Exception $e) {
		return array('error' => $mail->ErrorInfo);
	}
	return array();
/*
	global $error;
	$mail = new PHPMailer();
	$mail->IsSMTP();		// Ativar SMTP
	$mail->SMTPDebug = 0;		// Debugar: 1 = erros e mensagens, 2 = mensagens apenas
	$mail->SMTPAuth = true;		// Autenticação ativada
	$mail->SMTPSecure = 'ssl';	// SSL REQUERIDO pelo GMail
	$mail->Host = 'smtp.gmail.com';	// SMTP utilizado
	$mail->Port = 587;  		// A porta 587 deverá estar aberta em seu servidor
	$mail->Username = GUSER;
	$mail->Password = GPWD;
	$mail->SetFrom($de, $de_nome);
	$mail->Subject = $assunto;
	$mail->Body = $corpo;
	$mail->AddAddress($para);
	if(!$mail->Send()) {
		$error = 'Mail error: '.$mail->ErrorInfo;
		return false;
	} else {
		$error = 'Mensagem enviada!';
		return true;
	}
*/
}
function base64url_decode($data) {
    $data = strtr($data, '-_', '+/');
    echo "<pre>base64url_decode ".var_export($data, true)."</pre>";
    return base64_decode($data);
  // return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}
$_has_session = [];
function has_session($uid) {
    global $_has_session;
    $sessions = [];
    if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));
    $session = [];
    if (@array_key_exists($uid, $sessions)) {
       $session = $sessions[$uid];
    }
    return (@array_key_exists('local.private', $session));
}
$_local_decrypt = [];
function local_decrypt($uid, $encrypted) {
    global $_local_decrypt;
    $sessions = array();
    if (file_exists('sessions')) $sessions = unserialize(file_get_contents('sessions'));
    $ini = ini_get( 'display_errors');
    //ini_set( 'display_errors', '0');
    try {
	$_local_decrypt[] = ['$uid' => $uid];
        if (!empty($uid)) {
            // procura sessao
            $session = array();
            if (@array_key_exists($uid, $sessions)) {
                $session = $sessions[$uid];
            	//echo "<pre>session ".var_export($session, true)."</pre>";
	    }
	    $_local_decrypt[] = ['session' => $session];
            //echo "<pre>\n".var_export($session, true)."\n</pre>";
            //echo "\n\n";
            if (@array_key_exists('local.private', $session)) {

                //echo "<pre>\n".var_export($session['local.private'], true)."\n</pre>";

                $rsaPrivateKey = new RSA();
            	$rsaPrivateKey->loadKey($session['local.private']);

                $rsaPrivateKey->setSignatureMode(RSA::SIGNATURE_PKCS1);
                //$rsaPublicKey->setEncryptionMode(RSA::ENCRYPTION_PKCS1);

            	//echo "<pre>rsaPublicKey teseu ".var_export(base64_encode($rsaPublicKey->encrypt("teseu")), true)."</pre>";
            	//echo "<pre>rsaPublicKey 123456 ".var_export(base64_encode($rsaPublicKey->encrypt("123456")), true)."</pre>";

            	//$test = "VdD/yAP3usdaY2vVNyrSnYrvFyuRuurHikTL2Vm40MLKkMBLvU5W4k0sTYA8vdYYmdllWnn+dEvk8yo20y9Kb03gJVHKvlZGAEaKdJOWMMgHqRdDcEXCpFXZ8E63kDE7";
            	//echo "<pre>test encrypted ".var_export($test, true)."</pre>";
            	//echo "<pre>test decrypted ".var_export($rsaPrivateKey->decrypt(base64_decode($test)), true)."</pre>";

            	//echo "<pre>encrypted ".var_export(strtr($encrypted, '-_', '+/'), true)."</pre>";
            	//echo "<pre>encrypted ".var_export($rsaPrivateKey->decrypt(base64url_decode($encrypted)), true)."</pre>";
                return $rsaPrivateKey->decrypt(base64_decode(strtr($encrypted, '-_', '+/')));
            }
        }
    } catch(Exception $e) {
        return $e->getMessage();
    } finally {
        ini_set( 'display_errors', $ini );
    }
	return null;
}
function __head($head = []) { ?>
  <head>
    <meta charset="UTF-8">
    <title><? echo (@$head['title'] ?? 'Re:volution'); ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<? }
function __form($form = []) { ?>
  <form action="<?php echo (@$form['action']) ?? '/'; ?>" method="post">
    <? foreach ((@$form['input'] ?? []) as $key => $value) : ?>
    <div class="form-group has-feedback">
      <?php if (@$value['invalid']) : ?>
       <div class="input row-input with-error has-error text-red"><? echo $value['invalid']; ?></div>
      <?php endif; ?>
      <?php if (@$value['text']) : ?>
       <div class="input row-input"><? echo $value['text']; ?></div>
      <?php endif; ?>
      <input  type="<? echo (@$value['type'] ?? 'text'); ?>"
              class="form-control"
              <? if (@$value['placeholder']) : ?>
              placeholder="<? echo $value['placeholder']; ?>"
              <? endif; ?>
              name="<? echo @$value['name'] ?? 'test'; ?>"
              <? if (@$value['required']) : ?>
              required
              <? endif; ?>
              value="<? echo @$value['value'] ?? ''; ?>" />
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <? endforeach; ?>
    <div class="row">
      <div class="col-xs-8">
        <input  type="submit"
                class="btn btn-primary btn-block btn-flat"
                value="<? echo (@$form['submit']['value'] ?? 'ENVIAR') ; ?>" />
      </div><!-- /.col -->
    </div>
  </form>
<? }
function __body($body = []) { ?>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <!-- Login  -->
      </div><!-- /.login-logo -->
      <div class="login-box-body">
      	<img src="/images/revolution_logo.png" width="320px;"><br/><br/>
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
        <? __form(@$body['form'] ?? []) ?>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    <script src="/js/jQuery-2.1.4.min.js"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
  </body>
<? }

function __html($html) { ?>
  <!DOCTYPE html>
  <html>
  <? __head(@$html['head'] ?? []); ?>
  <? __body(@$html['body'] ?? []); ?>
  </html>
<? }

// Routes
// -------------------------
// ---------------------------------------------> v2
