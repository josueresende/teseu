<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;

/**
 */
$app->post('/phaedra/register', function (Request $request, Response $response) use ($app) {

    $db = $this->get('database-connection');

    $data['error_code'] = 0;
    $invalid = null;
    $user = [];

    try {
        $form = $request->getParsedBody();

        $username = md5(sha1($form['username']));
        $password = md5(md5($form['password']));

        {
            $stmt = $db->prepare("SELECT * FROM usuario WHERE login LIKE :login ");
            $stmt->bindParam(":login", $username);
            $stmt->execute();
            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $user = $users[0];
        }

        if ($user['_enabled'] == 'S') {
            // usuario ja registrado
            $data['error_code'] = 999;
            $data['error_description'] = "Esse usuário já está registrado.";
        } else if ($user['_enabled'] == 'N') {
            // revalidacao
            $sql = "UPDATE usuario SET tipo = :tipo, senha = :senha WHERE id = :id;";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id", $user['id']);
            $stmt->bindParam(":tipo", $form['tipo']);
            $stmt->bindParam(":senha", $password);
            $stmt->execute();
        } else if (filter_var($form['username'], FILTER_VALIDATE_EMAIL)) {
            // cadastro
            $sql = "INSERT INTO usuario (tipo, login, nome, senha) VALUES (:tipo, :login, :nome, :senha);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":tipo", $form['tipo']);
            $stmt->bindParam(":nome", $form['username']);
            $stmt->bindParam(":login", $username);
            $stmt->bindParam(":senha", $password);
            $stmt->execute();
            $user['id'] = $db->lastInsertId();
        } else {
            // email invalido
            $data['error_code'] = 999;
            $data['error_description'] = "Utilize um e-mail válido.";
        }

        if (($data['error_code'] == 0) and ($user['id'] > 0)) {
            // invalidar tokens anteriores
            $sql =  "UPDATE token SET token.data_delecao = now() WHERE token.acao = 'USER_REGISTER' and token.id_usuario = :id_usuario ";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $user['id']);
            $stmt->execute();
            // criar token de reset com validade
            $hash = sha1(md5(uniqid(rand(), true)));
            $expiracao = new DateTime('now');
            $expiracao = $expiracao->modify('+1 day');
            $dataDeDelecao = $expiracao->format("Y-m-d H:i:s");
            $sql = "INSERT INTO token (id_usuario, data_delecao, acao, hash) VALUES (:id_usuario, :data_delecao, 'USER_REGISTER', :hash);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $user['id']);
            $stmt->bindParam(":data_delecao", $dataDeDelecao);
            $stmt->bindParam(":hash", $hash);
            $stmt->execute();
            // enviar e-mail
            $link = $_SERVER['HTTP_X_URL'].'/'.$hash;
            mailer($form['username'],
                "Registro na Plataforma",
                "<p>Ol&aacute;,</p>".
                "<p style='padding-left: 40px;'>Obrigado por ter se inscrito em nossa plataforma como {$form['tipo']}.</p>".
                "<p style='padding-left: 40px;'>Desejamos que a utilize da melhor forma poss&iacute;vel.</p>".
                "<p style='padding-left: 40px;'>Clique no <a href='$link'>link</a> para completar esta a&ccedil;&atilde;o.</p>".
                "<p>Atenciosamente</p>".
                "<p>Equipe de Desenvolvimento</p>"
            );
            $data['error_description'] = "Foi enviado um email com link de registro.";
        }

    } catch(Exception $e) {
        $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }

    return $response->withJson($data, 200)
        ->withHeader('Content-type', 'application/json');
});
