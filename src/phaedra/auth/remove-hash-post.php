<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'remove-hash.php';

$app->post('/phaedra/remove/{hash}', function (Request $request, Response $response, array $args) {
    $token['id_usuario'] = -1;
    $form = $request->getParsedBody();
    try {
        $sql =
            " SELECT token.id_usuario, usuario.senha, token.acao, token.hash
          FROM token
          INNER JOIN usuario ON usuario.id = token.id_usuario
          WHERE token.acao = 'USER_COMPLETE_REMOVE' AND token.hash = :hash AND now() < token.data_delecao ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":hash", $args['hash']);
        $stmt->execute();
        $tokens = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($tokens) == 0) return $response->withStatus(403);
        $token = $tokens[0];
        $data['debug'][] = ['$tokens' => $tokens, '$token' => $token];
    } catch (Exception $e) {
        //
    }

    $parametros = ['hash' => $args['hash'],'email' => $form['email']];

    // error_log(var_export([
    //     'password-from-page' => $form['password'],
    //     'password' => md5(md5($form['password'])),
    //     'password-from-bd' => $token['senha']
    // ], true));

    // if ($form['password'] != $form['password-confirm']) $parametros['password-invalido'] = true;

    // if (filter_var($form['email'], FILTER_VALIDATE_EMAIL) != $form['email']) $parametros['email-invalido'] = true;
    if (md5(md5($form['password'])) != $token['senha']) {
        $parametros['password-invalido'] = true;
        phaedra_formUserRemover($parametros);
        return $response->withStatus(200);
    }

    try {
        // $sql =  "UPDATE token SET token.data_delecao = now() WHERE token.hash = :hash ";
        // $db = getConnection();
        // $stmt = $db->prepare($sql);
        // $stmt->bindParam(":hash", $token['hash']);
        // $stmt->execute();

        /*
        DELETE FROM resposta_buscada WHERE resposta_buscada.id_busca IN (SELECT id FROM busca WHERE busca.id_usuario = 29);
        DELETE FROM busca WHERE busca.id_usuario = 29;
        DELETE FROM resposta_buscada WHERE resposta_buscada.id_mensagem_anuncio IN (SELECT mensagem_anuncio.id FROM mensagem_anuncio INNER JOIN mensagem ON mensagem.id = mensagem_anuncio.id_mensagem WHERE mensagem.id_usuario = 29);
        DELETE FROM mensagem_anuncio WHERE mensagem_anuncio.id_mensagem IN (SELECT mensagem.id FROM mensagem WHERE mensagem.id_usuario = 29);
        DELETE FROM mensagem WHERE mensagem.id IN (SELECT mensagem_direta.id_mensagem FROM mensagem_direta WHERE mensagem_direta.id_usuario = 29);
        DELETE FROM mensagem_direta WHERE mensagem_direta.id_usuario = 29;
        DELETE FROM mensagem_direta WHERE mensagem_direta.id_mensagem = (SELECT mensagem.id FROM mensagem WHERE mensagem.id_usuario = 29)

        REMOVER COMPLETAMENTE
         */
        $auditoria =[];

        {
            $sql =  "SELECT * FROM resposta_buscada WHERE resposta_buscada.id_busca IN (SELECT id FROM busca WHERE busca.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['resposta_buscada'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $sql =  "SELECT * FROM busca WHERE busca.id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['busca'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $sql =  "SELECT * FROM resposta_buscada WHERE resposta_buscada.id_mensagem_anuncio IN (SELECT mensagem_anuncio.id FROM mensagem_anuncio INNER JOIN mensagem ON mensagem.id = mensagem_anuncio.id_mensagem WHERE mensagem.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['criterios_de_anuncio'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $sql =  "SELECT * FROM mensagem_anuncio WHERE mensagem_anuncio.id_mensagem IN (SELECT mensagem.id FROM mensagem WHERE mensagem.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['anuncio'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $sql =  "SELECT * FROM mensagem WHERE mensagem.id IN (SELECT mensagem_direta.id_mensagem FROM mensagem_direta WHERE mensagem_direta.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['mensagens_recebidas'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $sql =  "SELECT * FROM mensagem WHERE mensagem.id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['mensagens_enviadas'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        // {
        //     $sql =  "SELECT * FROM mensagem_direta WHERE mensagem_direta.id_usuario = :id_usuario);";
        //     $db = getConnection();
        //     $stmt = $db->prepare($sql);
        //     $stmt->bindParam(":id_usuario", $token['id_usuario']);
        //     $stmt->execute();
        //     $auditoria['recebidas'] = json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
        // }
        {
            $sql =  "SELECT * FROM resposta_marcada WHERE id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['respostas'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $sql =  "SELECT * FROM usuario WHERE id = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
            $auditoria['usuario'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        {
            $log = json_encode($auditoria);
            error_log(var_export($log, true));
            // usuario
            $sql = "INSERT INTO auditoria (acao, log) VALUES ('USER_COMPLETE_REMOVE', :log);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":log", $log);
            $stmt->execute();
        }
        {

        }
        {
            $sql =  "DELETE FROM resposta_buscada WHERE resposta_buscada.id_busca IN (SELECT id FROM busca WHERE busca.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM busca WHERE busca.id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM resposta_buscada WHERE resposta_buscada.id_mensagem_anuncio IN (SELECT mensagem_anuncio.id FROM mensagem_anuncio INNER JOIN mensagem ON mensagem.id = mensagem_anuncio.id_mensagem WHERE mensagem.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM mensagem_anuncio WHERE mensagem_anuncio.id_mensagem IN (SELECT mensagem.id FROM mensagem WHERE mensagem.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM mensagem WHERE mensagem.id IN (SELECT mensagem_direta.id_mensagem FROM mensagem_direta WHERE mensagem_direta.id_usuario = :id_usuario);";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM mensagem WHERE mensagem.id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM mensagem_direta WHERE mensagem_direta.id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM resposta_marcada WHERE id_usuario = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }
        {
            $sql =  "DELETE FROM usuario WHERE id = :id_usuario;";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $token['id_usuario']);
            $stmt->execute();
        }

        phaedra_formUserRemoverOk();

    } catch (Exception $e) {
        //
    }

    return $response->withStatus(200);
});
