<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;

include_once 'register-hash.php';

$app->post('/phaedra/register/{hash}', function (Request $request, Response $response, array $args) use ($app) {

    $db = $this->get('database-connection');

    $form = $request->getParsedBody();

    try {
        $sql = "SELECT token.id_usuario, token.acao, usuario.senha, token.hash
                FROM token
                INNER JOIN usuario ON usuario.id = token.id_usuario
                WHERE token.acao = 'USER_REGISTER' AND token.hash = :hash AND now() < token.data_delecao ";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":hash", $args['hash']);
        $stmt->execute();
        $tokens = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($tokens) == 0) return $response->withStatus(403);
        $token = $tokens[0];
        $data['debug'][] = ['$tokens' => $tokens, '$token' => $token];
    } catch (Exception $e) {
        //
    }

    if (md5(md5($form['password'])) != $token['senha']) {
        $parametros['password-invalido'] = true;
        phaedra_formUserRegister($parametros);
        return $response->withStatus(200);
    }

    try {
        $sql =  "UPDATE token SET token.data_delecao = now() WHERE token.hash = :hash ";

        $stmt = $db->prepare($sql);
        $stmt->bindParam(":hash", $token['hash']);
        $stmt->execute();

        $sql =  "UPDATE usuario SET usuario._enabled = 'S' WHERE usuario.id = :id_usuario ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":id_usuario", $token['id_usuario']);
        $stmt->execute();

        phaedra_formUserRegisterOk();

    } catch (Exception $e) {
        //
    }

    return $response->withStatus(200);
});
