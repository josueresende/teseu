<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;

function phaedra_formUserRemoverOk() { ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login-page">
    <div class="login-box">
        <div class="login-logo">
            <!-- Login  -->
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <img src="/images/revolution_logo.png" width="320px;"><br/><br/>
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    Seu perfil foi removido completamente.
                </div>
            </div>
        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    <script src="/js/jQuery-2.1.4.min.js"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
    </html>
<?php }

function phaedra_formUserRemover($config) { ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login-page">
    <div class="login-box">
        <div class="login-logo">
            <!-- Login  -->
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <img src="/images/revolution_logo.png" width="320px;"><br/><br/>
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
            <form action="/phaedra/remove/<?php echo $config['hash']; ?>" method="post">
                <div class="form-group has-feedback">
                    Voc&ecirc; ser&aacute; removido completamente da plataforma.
                </div>
                <div class="form-group has-feedback">
                    <?php if ($config['password-invalido']) : ?>
                        <div class="input row-input with-error has-error text-red">Senha incorreta, tente novamente.</div>
                    <?php endif; ?>
                    <input type="password" class="form-control" placeholder="Password" name="password" required />
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <!-- <div class="checkbox icheck">
                          <label>
                            <input type="checkbox"> Remember Me
                          </label>
                        </div>  -->
                    </div><!-- /.col -->
                    <div class="col-xs-8">
                        <input type="submit" class="btn btn-primary btn-block btn-flat" value="REMOVER USUARIO" />
                    </div><!-- /.col -->
                </div>
            </form>
        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    <script src="/js/jQuery-2.1.4.min.js"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
    </html>
<?php }
