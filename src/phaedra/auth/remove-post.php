<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;

/**
 */
$app->post('/phaedra/remove', function (Request $request, Response $response) use ($app) {

    $db = $this->get('database-connection');

    $data['error_code'] = 0;
    $invalid = null;
    $user = [];

    try {
        $form = $request->getParsedBody();

        $username = md5(sha1($form['username']));

        {
            $stmt = $db->prepare("SELECT * FROM usuario WHERE login LIKE :login ");
            $stmt->bindParam(":login", $username);
            $stmt->execute();
            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $user = $users[0];
        }

        if (is_null($user['id'])) {
            return $response->withStatus(503);
        }

        if (($data['error_code'] == 0) and ($user['id'] > 0)) {
            // invalidar tokens anteriores
            $sql =  "UPDATE token SET token.data_delecao = now() WHERE token.acao = 'USER_COMPLETE_REMOVE' and token.id_usuario = :id_usuario ";
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $user['id']);
            $stmt->execute();
            // criar token de reset com validade
            $hash = sha1(md5(uniqid(rand(), true)));
            $expiracao = new DateTime('now');
            $expiracao = $expiracao->modify('+1 day');
            $dataDeDelecao = $expiracao->format("Y-m-d H:i:s");
            $sql = "INSERT INTO token (id_usuario, data_delecao, acao, hash) VALUES (:id_usuario, :data_delecao, 'USER_COMPLETE_REMOVE', :hash);";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id_usuario", $user['id']);
            $stmt->bindParam(":data_delecao", $dataDeDelecao);
            $stmt->bindParam(":hash", $hash);
            $stmt->execute();
            // enviar e-mail
            $link = $_SERVER['HTTP_X_URL'].'/'.$hash;
            mailer($form['username'],
                "Solicitação de remoção da plataforma em ".date('d/m/Y'),
                "<p>Ol&aacute;,</p>".
                "<p style='padding-left: 40px;'>Voc&ecirc; solicitou recentemente sair completamente da plataforma.</p>".
                "<p style='padding-left: 40px;'>Clique no <a href='$link'>link</a> para completar esta a&ccedil;&atilde;o.</p>".
                "<p>Atenciosamente</p>".
                "<p>Equipe de Desenvolvimento</p>"
            );
            $data['error_description'] = "Foi enviado um email com link de remocao completa.";
        }

    } catch(Exception $e) {
        $data['error_code'] = 999;
        $data['error_description'] = $e->getMessage();
    }

    return $response->withJson($data, 200)
        ->withHeader('Content-type', 'application/json');
});
