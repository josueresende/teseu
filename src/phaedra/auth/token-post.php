<?php
use Slim\Http\Request;
use Slim\Http\Response;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;

/**
 * HTTP Auth - Autenticação minimalista para retornar um JWT
 */
$app->post('/phaedra/token', function (Request $request, Response $response) use ($app) {

    $db = $this->get('database-connection');

    error_log(var_export([
        __FILE__ => __LINE__,
        '$db' => $db,
    ], true));

    $invalid = null;
    $user = null;

    {
        $form = $request->getParsedBody();

        error_log(var_export([
            __FILE__ => __LINE__,
            '$form' => $form,
        ], true));

        if (empty($form['username']) or empty($form['password'])) {
            return $response->withStatus(403, "invalid user");
        }

        $sql = "SELECT id, nome as login, tipo, foto_base64, tester
                FROM usuario
                WHERE _enabled = 'S' AND usuario.login LIKE :login AND senha LIKE :senha "; // AND tipo = :tipo";

        $stmt = $db->prepare($sql);

        $_md5_sha1_username = md5(sha1($form['username']));
        $_md5_md5_password = md5(md5($form['password']));

        $stmt->bindParam(":login", $_md5_sha1_username);
        $stmt->bindParam(":senha", $_md5_md5_password);

        $stmt->execute();

        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $invalid = (count($users) == 0);

        $user = @$users[0];
    }

    error_log(var_export([
        __FILE__ => __LINE__,
        '$user' => $user,
    ], true));

    if ($invalid) {
        return $response->withStatus(403, "invalid user");
    }

    $payload = openssl_encrypt(json_encode([
        bin2hex(random_bytes(random_int(1, 9))) => bin2hex(random_bytes(random_int(1, 9))),
        'userid' => $user['id'],
        'username' => $user['login'],
        'usertype' => $user['tipo'],
        bin2hex(random_bytes(random_int(1, 9))) => bin2hex(random_bytes(random_int(1, 9))),
    ]), 'aes256', sha1($key, true));

    $key = $this->get("secretkey");
    $token = array(
        "payload" => $payload,
        "usertype" => $user['tipo'],
    );

    $jwt = \Firebase\JWT\JWT::encode($token, $key, 'HS512');

    return $response->withJson(["jwt" => $jwt], 200)
        ->withHeader('Content-type', 'application/json');
});
