<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;

include_once 'register-hash.php';

$app->get('/phaedra/register/{hash}', function (Request $request, Response $response, array $args) use ($app) {
    $db = $this->get('database-connection');
    $data['debug'][] = ['$args' => $args];
    try {
        $sql =  "SELECT token.id_usuario, token.acao " .
            "FROM token " .
            "WHERE token.acao = 'USER_REGISTER' AND token.hash = :hash " .
            "AND now() < token.data_delecao ";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":hash", $args['hash']);
        $stmt->execute();
        $tokens = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($tokens) == 0) return $response->withStatus(403);
        $token = $tokens[0];
        $data['debug'][] = ['$tokens' => $tokens, '$token' => $token];
        phaedra_formUserRegister($args);
    } catch (Exception $e) {
    }
    return $response->withStatus(200);
});
