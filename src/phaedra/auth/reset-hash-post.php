<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;
include_once 'reset-hash.php';

$app->post('/phaedra/reset/{hash}', function (Request $request, Response $response, array $args) {
    $form = $request->getParsedBody();
    try {
        $sql =  "SELECT token.id_usuario, token.acao, token.hash " .
            "FROM token " .
            // "INNER JOIN usuario ON usuario.id = token.id_usuario " .
            "WHERE token.acao = 'PASSWORD_RESET' AND token.hash = :hash " .
            "AND now() < token.data_delecao ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":hash", $args['hash']);
        $stmt->execute();
        $tokens = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($tokens) == 0) return $response->withStatus(403);
        $token = $tokens[0];
        $data['debug'][] = ['$tokens' => $tokens, '$token' => $token];
    } catch (Exception $e) {
        //
    }

    $parametros = ['hash' => $args['hash'],'email' => $form['email']];

    error_log(var_export($parametros, true));

    if ($form['password'] != $form['password-confirm']) $parametros['password-invalido'] = true;

    if (filter_var($form['email'], FILTER_VALIDATE_EMAIL) != $form['email']) $parametros['email-invalido'] = true;

    error_log(var_export($parametros, true));

    if (array_key_exists('email-invalido', $parametros) || array_key_exists('password-invalido', $parametros)) {
        phaedra_formPasswordReset($parametros);
        return $response->withStatus(200);
    }

    try {
        $sql =  "UPDATE token SET token.data_delecao = now() WHERE token.hash = :hash ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":hash", $token['hash']);
        $stmt->execute();

        $senha = md5(md5($form['password']));

        $sql =  "UPDATE usuario SET usuario._enabled = 'S', usuario.senha = :senha WHERE usuario.id = :id_usuario ";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":senha", $senha);
        $stmt->bindParam(":id_usuario", $token['id_usuario']);
        $stmt->execute();

        phaedra_formPasswordResetOk();

    } catch (Exception $e) {
        //
    }

    return $response->withStatus(200);
});
