<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;


class Resposta {
    public $id;
    public $tag;
    public $texto;
    public $conteudo;
    public $marcada;
    public function __construct($id, $tag, $texto, $conteudo, $marcada) {
        $this->id = $id;
        $this->tag = $tag;
        $this->texto = $texto;
        $this->conteudo = $conteudo;
        $this->marcada = $marcada;
    }
}

class Questao {
    public $id;
    public $tag;
    public $texto;
    public $limite_marcadas;
    public $config;
    public $is_criterio;
    public $texto_especifico;
    public $ordem_exibicao;
    public $respostas = [];
    public function __construct($id, $tag, $texto, $limite_marcadas, $config, $is_criterio, $texto_especifico, $ordem_exibicao) {
        $this->id = $id;
        $this->tag = $tag;
        $this->texto = $texto;
        $this->limite_marcadas = $limite_marcadas;
        $this->config = $config;
        $this->is_criterio = $is_criterio;
        $this->texto_especifico = $texto_especifico;
        $this->ordem_exibicao = $ordem_exibicao;
    }
    public function add(Resposta $resposta) {
        if (array_key_exists($resposta->id, $this->respostas) == false) {
            $this->respostas[$resposta->id] = $resposta;
        }
        return $this->respostas[$resposta->id];
    }
}

class Formulario {
    public $id;
    public $titulo;
    public $texto;
    public $resposta_referencia;
    public $resposta_conclusiva;
    public $questoes = [];
    public function __construct($id, $titulo, $texto, $resposta_referencia, $resposta_conclusiva) {
        $this->id = $id;
        $this->titulo = $titulo;
        $this->texto = $texto;
        $this->resposta_referencia = $resposta_referencia;
        $this->resposta_conclusiva = $resposta_conclusiva;
    }
    public function add(Questao $questao) {
        if (array_key_exists($questao->id, $this->questoes) == false) {
            $this->questoes[$questao->id] = $questao;
        }
        return $this->questoes[$questao->id];
    }
}

class Resultado {
    public $formularios = [];
    public function add(Formulario $formulario) {
        if (array_key_exists($formulario->id, $this->formularios) == false) {
            $this->formularios[$formulario->id] = $formulario;
        }
        return $this->formularios[$formulario->id];
    }
}

$app->post('/phaedra/forms/questionsandanswers', function (Request $request, Response $response) use ($app) {

    $db = $this->get('database-connection');

    $userid   = $request->getAttribute('userid');
    $username = $request->getAttribute('username');
    $usertype = $request->getAttribute('usertype');

    error_log(var_export([
        __FILE__ => __LINE__,
        '$userid' => $userid,
        '$username' => $username,
        '$usertype' => $usertype,
    ], true));

    $sql =
        "SELECT DISTINCT " .
            "formulario.id as formulario_id, " .
            "formulario.titulo as formulario_titulo, " .
            "formulario.resposta_texto_livre as formulario_texto, " .
            "formulario.id_resposta_referencia as formulario_id_resposta_referencia, " .
            "formulario.id_resposta_conclusiva as formulario_id_resposta_conclusiva, " .
            "questao.id as questao_id, " .
            "questao.tag as questao_tag, " .
            "questao.texto as questao_texto, " .
            "questao.limite_marcadas as questao_limite_marcadas, " .
            "questao.config as questao_config, " .
            "questao.is_criterio as questao_is_criterio, " .
            "questao.texto_especifico as questao_texto_especifico, " .
            "questao_formulario.ordem_exibicao as questao_ordem_exibicao, " .
            "resposta.id as resposta_id, " .
            "resposta.tag as resposta_tag, " .
            "resposta.texto as resposta_texto, " .
            "resposta.conteudo as resposta_conteudo, " .
            "resposta_marcada.texto as resposta_marcada_texto, " .
            "now() as now " .
            "FROM formulario " .
            "LEFT JOIN questao_formulario ON questao_formulario.id_formulario = formulario.id " .
            "LEFT JOIN questao ON questao.id = questao_formulario.id_questao " .
            "LEFT JOIN resposta ON resposta.id_questao = questao.id " .
            "LEFT JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id AND resposta_marcada.id_usuario = :id_usuario ".
            // "WHERE questao.id > 0 " .
            "ORDER BY formulario.id_resposta_referencia, questao.id, questao_formulario.ordem_exibicao, resposta.id"
    ;
    $stmt = $db->prepare($sql);
    $stmt->bindParam(":id_usuario", $userid);
    $stmt->execute();
    
    // $resultados = $stmt->fetchAll(PDO::FETCH_ASSOC); //  [];

    $Resultado = new Resultado();

    foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $key => $value) {
        $formulario_id = $value['formulario_id'];
        $questao_id = $value['questao_id'];
        $resposta_id = $value['resposta_id'];

        $Formulario = $Resultado->add(new Formulario(
            $formulario_id,
            $value['formulario_titulo'],
            $value['formulario_texto'],
            $value['formulario_id_resposta_referencia'],
            $value['formulario_id_resposta_conclusiva']
        ));

        $Questao = $Formulario->add(new Questao(
            $questao_id,
            $value['questao_tag'],
            $value['questao_texto'],
            $value['questao_limite_marcadas'],
            $value['questao_config'],
            $value['questao_is_criterio'],
            $value['questao_texto_especifico'],
            $value['questao_ordem_exibicao']
        ));

        $Resposta = $Questao->add(new Resposta(
            $resposta_id,
            $value['resposta_tag'],
            $value['resposta_texto'],
            $value['resposta_conteudo'],
            $value['resposta_marcada_texto']
        ));
    }

    error_log(var_export([
        __FILE__ => __LINE__,
        '$Resultado' => $Resultado,
    ], true));

    return $response->withJson($Resultado, 200)
        ->withHeader('Content-type', 'application/json');
});
