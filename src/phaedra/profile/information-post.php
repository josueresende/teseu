<?php
use Slim\Http\Request;
use Slim\Http\Response;
use phpseclib\Crypt\RSA;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Bluerhinos\phpMQTT;

$app->post('/phaedra/profile/information', function (Request $request, Response $response) use ($app) {

    $db = $this->get('database-connection');

    $userid   = $request->getAttribute('userid');
    $username = $request->getAttribute('username');
    $usertype = $request->getAttribute('usertype');

    error_log(var_export([
        __FILE__ => __LINE__,
        '$userid' => $userid,
        '$username' => $username,
        '$usertype' => $usertype,
    ], true));

    $sql = "SELECT * FROM usuario WHERE _enabled = 'S' AND usuario.id = :id";

    $stmt = $db->prepare($sql);

    $stmt->bindParam(":id", $userid);

    $stmt->execute();

    $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $user = @$users[0];

    unset($user['senha']);

    $sql =
        "SELECT DISTINCT " .
            "questao.id as questao_id, " .
            "questao.tag as questao_tag, " .
            "questao.texto as questao_texto, " .
            "questao.limite_marcadas as questao_limite_marcadas, " .
            "resposta.id as resposta_id, " .
            "resposta.tag as resposta_tag, " .
            "resposta.texto as resposta_texto, " .
            "resposta.conteudo as resposta_conteudo, " .
            "resposta.texto as resposta_texto, " .
            "resposta_marcada.id as marcada_id, " .
            "resposta_marcada.texto as marcada_texto, " .
            "now() as now " .
            "FROM questao " .
            "LEFT JOIN resposta ON resposta.id_questao = questao.id " .
            "LEFT JOIN resposta_marcada ON resposta_marcada.id_resposta = resposta.id " .
            "LEFT JOIN usuario ON usuario.id = resposta_marcada.id_usuario " .
            "WHERE usuario.id = :id_usuario " .
            "AND questao.tag IS NOT NULL " .
            "ORDER BY questao.id, resposta.id "
    ;
    $stmt = $db->prepare($sql);
    $stmt->bindParam(":id_usuario", $userid);
    $stmt->execute();
    $resultados = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $response->withJson([
        'size' => count($resultados),
        'results' => $resultados,
    ], 200)
        ->withHeader('Content-type', 'application/json');
});
